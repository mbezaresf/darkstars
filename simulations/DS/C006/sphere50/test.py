import numpy as np
import scipy.fftpack
import matplotlib.pyplot as pl
import math

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
 
       import numpy as np
       from math import factorial
       
       try:
           window_size = np.abs(np.int(window_size))
           order = np.abs(np.int(order))
       except ValueError, msg:
           raise ValueError("window_size and order have to be of type int")
       if window_size % 2 != 1 or window_size < 1:
           raise TypeError("window_size size must be a positive odd number")
       if window_size < order + 2:
           raise TypeError("window_size is too small for the polynomials order")
       order_range = range(order+1)
       half_window = (window_size -1) // 2
       # precompute coefficients
       b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
       m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
       # pad the signal at the extremes with
       # values taken from the signal itself
       firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
       lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
       y = np.concatenate((firstvals, y, lastvals))
       return np.convolve( m[::-1], y, mode='valid')


dataset = np.genfromtxt(fname='omega.dat')
x = dataset[:,0] 
y = dataset[:,1]/(2.0*math.pi) 
yhat = savitzky_golay(y, 3, 1)

dataset2 = np.genfromtxt(fname="/home/miguel/Programs/papers/darkstars/simulations/DS/C006/PN_tidal/t_omega_r.dat")
x2 = dataset2[:,0] 
y2 = dataset2[:,1]/(2.0*math.pi) 


pl.xlim(10.0, 1080.0)
pl.ylim(0.0, 0.05 )
#pl.plot(x,y)
pl.plot(x2,y2)
pl.plot(x,yhat, color='red')
pl.show()
