
!!! from arxiv:1301.3555

      program      main
      implicit     none
      
      integer :: i, iRec, N, order, flag, round
      real*16 :: m1,m2, r1,r2, M, sep_i, sep_f, c, tfactor, lambda1, lambda2
      real*16 :: pi, gammaCM, omega2CM, omegaCM, dt, tmax, r, nu, Momega
      real*16 :: xi1, xi2
      integer :: DTout, OUTPUTflag
      integer*4 :: Ntmax
      real*16 :: glitchFreq
      integer :: glitchesF
      integer,parameter :: rseed = 16264
      real*16 :: h22factor(3), Ih22factor(3), phase, phaseold, phasek(4), tmid
      real*16 :: psi422, Ipsi422

      ! R-K 4th order
      real*16, dimension(1:4) :: kx
      real*16 :: sepnID, x, t, xold


      logical :: ltrace
      parameter ( ltrace = .true.)

      real*16 :: G

!------------------------------------------------------
      ! PARAMETERS
      
      pi     = acos(-1.0d0)
      
      c      = 1.0
      G      = 1.0

      ! masses
      !q=1
!      m1 = 1.35
!      m2 = 1.35
      !q=0.85 
!      m1 = 1.15
!      m2 = 1.55
      !q=0.763
!      m1 = 1.03
!      m2 = 1.67
      m1 = 0.5
      m2 = 0.5


      ! adimensional spins
      xi1 = 0.0
      xi2 = 0.0
      
      
      ! radius (in code units) for SFHo,DD2,NL3 and q=1 are 8.0,8.66,9.66
      ! radius (in code units) for SFHo,DD2,NL3 and q=0.85, Mhigh are 7.82,8.52,9.50
      ! radius (in code units) for SFHo,DD2,NL3 and q=0.85, Mlow are 8.18,8.79,9.85
      ! radius (in code units) for DD2 and q=0.76, Mhigh are 8.44     
      ! radius (in code units) for DD2 and q=0.76, Mlow are 8.87
      r1 = 3.88
      r2 = 3.88
      
      sep_i = 10
      sep_f =  2.22
      dt    = 0.001    

      Ntmax = 100000000      
      tfactor = 4.94e-3 ! in msec

      DTout = 1000

!------------------------------------------------------

      ! compute initial separation
      r = sep_i
      M  = m1 + m2
      nu = (m1*m2)/(M**2.0)
      gammaCM = (G*M)/(r*(c**2))

      !!! determining initial pars in case of ^^circular motion^^
      omega2CM = ((G*M)/(r**3))*(1.0d0 + (- 3.0d0 + nu)*gammaCM + (6.0d0 + 10.25*nu + nu**2)*gammaCM**2)
      omegaCM = sqrt(omega2CM)
      print*, "r=", r, "mass=", M
      print*, "approximate Omega_2PN=", omegaCM

 
      x = (M*omegaCM)**(2./3.)
      xold = x
      phaseold = 0.0d0
      phase = phaseold

      print*, "x initial=", x
      print*, "phase initial=", phase

!      pause      
      
      open(unit=211, file="tx.dat")
      open(unit=212, file="t_omega_r.dat")      
      open(unit=213, file="h22.dat")
      open(unit=214, file="psi422.dat")
      open(unit=215, file="Ipsi422.dat")

      print*,"entering loop..."
      !!! MAIN LOOP...
      i=-1
      !do i=0, Ntmax
      do while(r .GE. sep_f) 
!      do while(i .LE. 1000) 
        i=i+1
        t = dt*(i+1)

        !!! R-K 4th order... y(n+1)=y(n)+(1/6)*dt*(k1+ 2*k2 + 2*k3+ k4)
        !! first K1 for RK-4th order

	phasek(1) = (x**(1.5))/M
        call NEWrhs_ST(m1, m2, xi1, xi2, r1, r2, x, kx(1) )

        ! rhs computed at.. k1
        x = xold + 0.5*dt*kx(1)
	phase = phaseold + 0.5*dt*phasek(1)

        !! second K2 for RK-4th order
	phasek(2) = (x**(1.5))/M
        call NEWrhs_ST(m1, m2, xi1, xi2, r1, r2, x, kx(2) )

        ! rhs computed at ... k2
        x = xold + 0.5*dt*kx(2)
	phase = phaseold + 0.5*dt*phasek(2)

        !! third K3 for RK-4th order
	phasek(3) = (x**(1.5))/M
        call NEWrhs_ST(m1, m2, xi1, xi2, r1, r2, x, kx(3) )
        
        ! rhs computed at ... k3
        x = xold + dt*kx(3)
	phase = phaseold + dt*phasek(3)

        !! fourth K4 for RK-4th order
	phasek(4) = (x**(1.5))/M
        call NEWrhs_ST(m1, m2, xi1, xi2, r1, r2, x, kx(4) )

        ! rhs computed at ... k4
        x = xold +  (1./6.)*dt*( kx(1) + 2.0*kx(2) + 2.0*kx(3) + kx(4) )  
	phase = phaseold + (1./6.)*dt*( phasek(1) + 2.0*phasek(2) + &
     &                                  2.0*phasek(3) + phasek(4) )

        ! compute frequency omega and separation
        ! using x= (M*omega)^(2/3) and omega^2*r^3=M
        omegaCM = (x**(1.5))/M
        r       = (M/omegaCM**2)**(1.0/3.0)
        


!need to keep updating things here so as to
!take the 2nd derivative
	if (i.eq.0) then
	  h22factor(1) = x * cos(2.*phase)

	  Ih22factor(1) = x * sin(2.*phase)
	else if (i.eq.1) then
	  h22factor(2) = h22factor(1) 
	  h22factor(1) = x * cos(2.*phase)

	  Ih22factor(2) = Ih22factor(1) 
	  Ih22factor(1) = x * sin(2.*phase)

	else if (i.ge.2) then
	  h22factor(3) = h22factor(2) 
	  h22factor(2) = h22factor(1) 
	  h22factor(1) = x * cos(2.*phase)

	  Ih22factor(3) = Ih22factor(2) 
	  Ih22factor(2) = Ih22factor(1) 
	  Ih22factor(1) = x * sin(2.*phase)
	 
	  tmid = t - dt
	  psi422 = (h22factor(1)-2.0d0*h22factor(2)+h22factor(3))/dt**2

	  Ipsi422 = (Ih22factor(1)-2.0d0*Ih22factor(2)+Ih22factor(3))/dt**2

	end if  
        
        ! update the x
        xold = x
	phaseold = phase              
        Momega = (x**(1.5))                
        

        ! dumping the data into the output files
        if (MOD(i,DTout).eq.0) then
           print*, "time=", t, "r", r
           write(211,100) t/M, x
           write(212,100) t/M, Momega, r
           write(213,100) tmid/M, h22factor(2), Ih22factor(2)
           write(214,100) tmid/M, psi422*M**2
           write(215,100) tmid/M, Ipsi422*M**2
        endif



      end do

200   continue

      close(unit=211)
      close(unit=212)
      close(unit=213)
      close(unit=214)
      close(unit=215)

!  100 format(16e16.8)
  100 format(16f20.12)
  111 format(16d30.15)

      end program


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! from arxiv:1301.3555
!!!

      subroutine NEWrhs_ST(m1,m2,xi1,xi2, r1,r2, x, kx)

      implicit none
      ! old parameters...

      real*16  :: m1,m2,xi1,xi2,r1,r2,kx,x,Fpp,Ftidal,line1,line2,line34,line5
      real*16  :: M, nu, pi, gamma, C, k2, xi, lambda1, lambda2, Ftidal1,Ftidal2
      real*16  :: a0,a1,a2,a3,a4,a5,a6,a7
      logical :: ltrace
      parameter ( ltrace = .true.)

      pi     = acos(-1.0d0)
      gamma  = 0.577126
      
      M  = m1 + m2
      nu = (m1*m2)/(M**2)
      xi = (xi1*m1 + xi2*m2)/M 
      
      ! point particle terms (main contribution) 
!      line1 = 1.0 - ( (743.0/336.0) + (11.0*nu/4.0) )*x + 4.0*pi*(x**(3./2.))
!      line2 = ( (34103./18144.) + (13661.*nu/2061.) + (59.*nu*nu/18.) )*(x**2.) &
!            - ( (4159./672.) + (189.*nu/8.) )*pi*(x**(5./2.)) 
!      line34 = ( (16447322263./139708800.) - (1712.*gamma/105.) - (56198689.*nu/217728.)  &
!             + (541.*nu*nu/896.) - (5605.*(nu**3)/2592.) + (pi*pi/48.)*(256.+ 451.*nu) &
!             - (856./105.)*log(16.*x) )*(x**3.) 
!      line5  = ( (-4415./4032.) + (358675.*nu/6048.) +  (91495.*nu*nu/1512.) )*pi*(x**(7./2.))

      ! from (3.6) and eq(A3) from the paper arxiv:1005.3306 
      a0 = 1.0
      a1 = 0.0
      a2 = - (743.0/336.0) + (11.0*nu/4.0) 
      a3 = 4.0*pi - 113.0*xi/12.0 + 19.0*nu*(xi1 + xi2)/6.0
      a4 = (34103.0/18144.0) + 5.0*xi*xi + nu*(13661.0/2016.0 - xi1*xi2/8.0) + (59.*nu*nu/18.)
      a5 = - pi*(4159.0/672.0 + 189.0*nu/8.0) - xi*(31571.0/1008.0 - 1165.0*nu/24.0) &
         + (xi1 + xi2)*(21863.0*nu/1008.0 - 79.0*nu*nu/6.0) - 3.0*(xi**3)/4.0 &
         + 9.0*nu*xi*xi1*xi2/4.0
      a6 = (16447322263./139708800.) - (1712.*gamma/105.) - (56198689.*nu/217728.)  &
         + (541.*nu*nu/896.) - (5605.*(nu**3)/2592.) + (pi*pi/48.)*(256.+ 451.*nu) &
         - (856./105.)*log(16.*x) &  
         - 80.0*pi*xi/3.0 + (20.0*pi/3.0 - 1135.0*xi/36.0)*nu*(xi1 + xi2) &
         + (64153.0/1008. - 457.0*nu/36.0)*xi*xi &
         - (787.0*nu/144.0 - 3037.0*nu*nu/144.0)*xi1*xi2
      a7 = -pi*(4415./4032. - 358675.*nu/6048. + 91495.*nu*nu/1512.) &
         - xi*(2529407.0/27216.0 - 845827.0*nu/6048.0 + 41551.0*nu*nu/864.0) &
         + (xi1 + xi2)*(1580239.0*nu/54432.0 - 451597.0*nu*nu/6048.0 + 2045.0*(nu**3)/432.0 &
                        + 107.0*nu*xi*xi/6.0 - 5.0*nu*nu*xi1*xi2/24.0 ) &
         + 12.0*pi*xi*xi - (xi**3)*(1505.0/24.0 + nu/8.0) + xi*xi1*xi2*(101.0*nu/24. + 3.0*nu*nu/8.0)

      ! dx/dt = 64*nu/5 * x^5 * Sum_k [ a_k * x^(k/2) ]
      Fpp = ((64.*nu)/(5.*M))*(x**5.0) * ( a0 + a1*(x**0.5) + a2*(x**1.0) + a3*(x**1.5) &
          + a4*(x**2.0) + a5*(x**2.5) + a6*(x**3.0) + a7*(x**3.5)   )

      ! the tidal terms from Flanagan PRD83, 084051 (2011)      
      !                      Shibata PRD87, 044001 (2013)
      !k2 = 0.1
      lambda1 = 332.0!(2.0/3.0)*k2*(r1**5.0)
      lambda2 = 332.0!(2.0/3.0)*k2*(r2**5.0)

      Ftidal1 = 32.0*m1*lambda2/(5.0*(M**7.0)) * ( 12.0*(1.0 + 11.0*m1/M)*(x**10.0) &
      + (4421.0/28.0 - (12263.0/28.0)*(m2/M) + (1893.0/2.0)*(m2/M)**2.0 - 661.0*(m2/M)**3.0)*(x**11.0) ) 
      Ftidal2 = 32.0*m2*lambda1/(5.0*(M**7.0)) * ( 12.0*(1.0 + 11.0*m2/M)*(x**10.0) &
      + (4421.0/28.0 - (12263.0/28.0)*(m1/M) + (1893.0/2.0)*(m1/M)**2.0 - 661.0*(m1/M)**3.0)*(x**11.0) ) 
      Ftidal = Ftidal1 + Ftidal2

       ! this was valid only for the equal mass case
!      C  = m1/r1
!      k2 = 0.1
!      Ftidal = (52.0/(5.0*M))*(x**10.)*(k2/C**5)*(1.0 + 5203.0*x/4368.0)
      
      kx = Fpp + Ftidal

      end subroutine NEWrhs_ST


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!





