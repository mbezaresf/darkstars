import sys, getopt
import os
import numpy as np
import h5py
from scipy import integrate
import math

def readfield(folder, field):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data

def surface_integral2(samples, h_theta, h_phi):
    integral = 0.0
  
    iphi = 0
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi]
     
    iphi = samples.shape[1] 
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta 
    integral = integral + np.sin(phi)*samples[itheta-1, iphi-1]

    iphi = 0
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta-1, iphi]

    iphi = samples.shape[1] 
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi-1]
    
    for iphi in xrange(1, 80):
        phi = iphi*h_phi
        for itheta in xrange(1, 160):
            theta = itheta*h_theta 
            integral = integral + 4.0*np.sin(phi)*samples[itheta, iphi]


    iphi = 0
    phi = iphi*h_phi
    for itheta in xrange(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]

    iphi = samples.shape[1] 
    phi = iphi*h_phi
    for itheta in xrange(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi-1]

    itheta = 0
    theta = itheta*h_theta  
    for iphi in xrange(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]       

    itheta = samples.shape[0] 
    theta = itheta*h_theta    
    for iphi in xrange(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta-1, iphi]           

    return 0.25*h_theta*h_phi*integral
      
def calculate(sphere, rext):
    dt = 1.5
    delta_output = 1
    massI = 1
    time_min = 0.0
    factor  = 1.0
    resolution_theta = 161
    resolution_phi = 81

    times = xrange(682)

    f110 = open(sphere + '/c2m2.dat', 'w')
    f111 = open(sphere + '/c2m1.dat', 'w')
    f112 = open(sphere + '/c20.dat', 'w')
    f113 = open(sphere + '/c2p1.dat', 'w')
    f114 = open(sphere + '/c2p2.dat', 'w')

    f120 = open(sphere + '/mass.dat', 'w')
    f121 = open(sphere + '/Jz.dat', 'w')
    f122 = open(sphere + '/phase.dat', 'w')

    f130 = open(sphere + '/omega.dat', 'w')
    f131 = open(sphere + '/c2p2_inf.dat', 'w')
    f132 = open(sphere + '/lum_gw.dat','w')                           

    f140 = open(sphere + '/lum_gw2p2.dat','w')                           
    f141 = open(sphere + '/lum_gw2p1.dat','w')                           
    f142 = open(sphere + '/lum_gw20.dat','w')                           
    f143 = open(sphere + '/lum_gw2m1.dat','w')                           
    f144 = open(sphere + '/lum_gw2m2.dat','w')                           

    psi4I = readfield(sphere + "/t0", 'psi4I')
    h_phi      = np.pi/resolution_phi
    h_theta      = 2*np.pi/resolution_theta

    #------computing the spin-weighted spherical harmonics Y'_l,m------------
    #--  Y_l,m = Y'_l,m * exp ( +i*m*phi) ---------------------------------
    #--  Y_l,m = Y'_l,m * [cos( +i*m*phi) + i*sin( +i*m*phi)] -------------
    #-- *Y_l,m = Y'_l,m * [cos( +i*m*phi) - i*sin( +i*m*phi)] -------------
    Y2m2 = np.zeros((2, resolution_theta, resolution_phi))
    Y2m1 = np.zeros((2, resolution_theta, resolution_phi))
    Y20 = np.zeros((2, resolution_theta, resolution_phi))
    Y2p1 = np.zeros((2, resolution_theta, resolution_phi))
    Y2p2 = np.zeros((2, resolution_theta, resolution_phi))

    c2m2 = np.zeros((2, len(times)))
    c2m1 = np.zeros((2, len(times)))
    c20 = np.zeros((2, len(times)))
    c2p1 = np.zeros((2, len(times)))
    c2p2 = np.zeros((2, len(times)))
    phase = np.zeros((len(times)))

    psi4r_tmp = np.zeros((resolution_theta, resolution_phi))
    psi4i_tmp = np.zeros((resolution_theta, resolution_phi))

    for iphi in xrange(resolution_phi):
        phi = iphi*h_phi
        for itheta in xrange(resolution_theta):
            theta = itheta*h_theta 

            tY2m2 = np.sqrt(5.0/(64.0*np.pi)) * (1.0 - np.cos(phi))**2
            tY2m1 =-np.sqrt(5.0/(16.0*np.pi)) * np.sin(phi) * (1.0 - np.cos(phi))
            tY20  = np.sqrt(15.0/(32.0*np.pi)) * (np.sin(phi))**2
            tY2p1 =-np.sqrt(5.0/(16.0*np.pi)) * np.sin(phi) * (1.0 + np.cos(phi)) 
            tY2p2 = np.sqrt(5.0/(64.0*np.pi)) * (1.0 + np.cos(phi))**2
                                
            Y2m2[0, itheta, iphi] = tY2m2*np.cos(-2.0*theta)
            Y2m1[0, itheta, iphi] = tY2m1*np.cos(-1.0*theta)
            Y20[0, itheta, iphi] = tY20*np.cos(0.0*theta)
            Y2p1[0, itheta, iphi] = tY2p1*np.cos(+1.0*theta)
            Y2p2[0, itheta, iphi] = tY2p2*np.cos(+2.0*theta)

            Y2m2[1, itheta, iphi] =-tY2m2*np.sin(-2.0*theta)
            Y2m1[1, itheta, iphi] =-tY2m1*np.sin(-1.0*theta)
            Y20[1, itheta, iphi]  =-tY20*np.sin(0.0*theta)
            Y2p1[1, itheta, iphi] =-tY2p1*np.sin(+1.0*theta)
            Y2p2[1, itheta, iphi] =-tY2p2*np.sin(+2.0*theta)

    for t in times:
        psi4r = readfield(sphere + "/t" + str(t*delta_output), 'psi4R')
        psi4i = readfield(sphere + "/t" + str(t*delta_output), 'psi4I')
        M_ADM_surf = readfield(sphere + "/t" + str(t*delta_output), 'M_ADM_surf')
        Jz_ADM_surf = readfield(sphere + "/t" + str(t*delta_output), 'Jz_ADM_surf')

        time = t*dt
        print "time=", time
         
        psi4r = psi4r*factor
        psi4i = psi4i*factor
                 
        temp1 = psi4r*Y2m2[0,:] - psi4i*Y2m2[1,:]
        temp2 = psi4r*Y2m2[1,:] + psi4i*Y2m2[0,:]
        c2m2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2m2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2m1[0,:] - psi4i*Y2m1[1,:]
        temp2 = psi4r*Y2m1[1,:] + psi4i*Y2m1[0,:]
        c2m1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2m1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y20[0,:] - psi4i*Y20[1,:]
        temp2 = psi4r*Y20[1,:] + psi4i*Y20[0,:]
        c20[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c20[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2p1[0,:] - psi4i*Y2p1[1,:]
        temp2 = psi4r*Y2p1[1,:] + psi4i*Y2p1[1,:]
        c2p1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2p1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2p2[0,:] - psi4i*Y2p2[1,:]
        temp2 = psi4r*Y2p2[1,:] + psi4i*Y2p2[0,:]
        c2p2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2p2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        tmass = surface_integral2(M_ADM_surf, h_theta, h_phi)
        tJz   = surface_integral2(Jz_ADM_surf, h_theta, h_phi)

        smallnumber =1e-6
        x = c2p2[0,t]
        y = c2p2[1,t]
        if x < smallnumber:
            phase[t] = math.atan(y/x) 
        elif ((x < -smallnumber) and (y >= 0.0)):
            phase[t] = math.atan(y/x)+pi
        elif ((x < -smallnumber) and (y < 0.0)):
            phase[t] = math.atan(y/x)-pi
        elif ((abs(x) < smallnumber) and (y > 0.0)):
            phase[t] = 0.5*pi
        elif ((abs(x) < smallnumber) and (y < 0.0)):
            phase[t] = -0.5*pi

        print "-------------------------"
        print " mass=", tmass
        print "-------------------------"      
        print "-----ANGULAR MOMENTUM----"
        print " Jz=", tJz      
        print "-------------------------"      
        print "-----L=2 MODES ----------"
        print "-------------------------"
        print "c2m2=",c2m2[0,t],"c2m2=",c2m2[1,t]
        print "c2m1=",c2m1[0,t],"c2m1=",c2m1[1,t]
        print "c20=",c20[0,t],  "c20=",c20[1,t]
        print "c2p1=",c2p1[0,t],"c2p1=",c2p1[1,t]
        print "c2p2=",c2p2[0,t],"c2p2=",c2p2[1,t]

        f110.write(str(time) + " " + str(c2m2[0, t]) + " " + str(c2m2[1, t]) + "\n")
        f111.write(str(time) + " " + str(c2m1[0, t]) + " " + str(c2m1[1, t]) + "\n")
        f112.write(str(time) + " " + str(c20[0, t]) + " " + str(c20[1, t]) + "\n")
        f113.write(str(time) + " " + str(c2p1[0, t]) + " " + str(c2p1[1, t]) + "\n")
        f114.write(str(time) + " " + str(c2p2[0, t]) + " " + str(c2p2[1, t]) + "\n")

        f120.write(str(time) + " " + str(tmass) + "\n")
        f121.write(str(time) + " " + str(tJz) + "\n")
        f122.write(str(time) + " " + str(phase[t]) + "\n")


    #-computing the frequency from the dominant mode l=m=2
    #-either by omega=dphase/dt
    # or omega = -1/m*Im [ (d C_{lm}/dt)/ C_{lm}] 
    # (a+i*b)/(c+i*d) = (a*c + b*d + i*(b*c - a*d))/(c^2 + d^2)

    for i in xrange(1, len(times) - 1):
        time = i*dt
        omega1 = 0.5*( phase[i+1]-phase[i-1] )/(2.0*dt)

        cR2p2   = c2p2[0, i]
        cI2p2   = c2p2[1, i]
        dtcR2p2 = (c2p2[0, i+1] - c2p2[0, i-1]) / (2.0*dt)
        dtcI2p2 = (c2p2[1, i+1] - c2p2[1, i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + " " + str(omega1) + "\n")


    #-nakano extrapolation to infinity
    # first define some constant
    ra        = rext * ( 1.0 + massI/(2.0*rext) )**2
    rr        = rext * ( 1.0 + 2.0*massI/rext )**2
    factor    = 4.0/(2.0*ra) 
    integralc = 0.0
    print "ra=",ra,"factor", factor 

    for i in xrange(1, len(times)):
        time = i*dt
        time_tort = time - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        if time > time_min:
            integralc = integralc + c2p2[:,i]*dt 
        c2p2_inf = (1.0 - 2.0*massI/ra)* (c2p2[:,i] - factor*integralc)
        f131.write(str(time_tort) + " " + str(c2p2_inf[0]) + " " + str(c2p2_inf[1]) + " " + str(time) + "\n")

    # compute the GW luminosity and total energy radiated in GW
    # first define some constant
    factor   = 1.0/(16.0*np.pi)
    d2p2     = 0.0
    d2p1     = 0.0
    d20      = 0.0
    d2m1     = 0.0
    d2m2     = 0.0
    print "ra=",ra,"factor", factor 

    for i in xrange(1, len(times)):
        time = i*dt
        time_tort = time - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        #with the raw data and the infinity extrapolation
        if time > time_min:
          d2p2 = d2p2 + c2p2[:,i]*dt 
          d2p1 = d2p1 + c2p1[:,i]*dt 
          d20  = d20  + c20[:,i]*dt 
          d2m1 = d2m1 + c2m1[:,i]*dt 
          d2m2 = d2m2 + c2m2[:,i]*dt 
        LGW    = factor*( d2p2[0]**2 + d2p2[1]**2 + d2p1[0]**2 + d2p1[1]**2 + d20[0]**2 + d20[1]**2 +  d2m1[0]**2 + d2m1[1]**2 + d2m2[0]**2 + d2m2[1]**2 )   
        f132.write(str(time_tort) + " " + str(LGW) + " " + str(time) + "\n")
        f140.write(str(time_tort) + " " + str(d2p2) + " " + str(time) + "\n")
        f141.write(str(time_tort) + " " + str(d2p1) + " " + str(time) + "\n")
        f142.write(str(time_tort) + " " + str(d20) + " " + str(time) + "\n")
        f143.write(str(time_tort) + " " + str(d2m1) + " " + str(time) + "\n")
        f144.write(str(time_tort) + " " + str(d2m2) + " " + str(time) + "\n")

    f110.close()
    f111.close()
    f112.close()
    f113.close()
    f114.close()

    f120.close()
    f121.close()
    f122.close()

    f130.close()
    f131.close()
    f132.close()

if __name__ == "__main__":
    for (sphere, rext) in [('data_outputDir_sphere50', 50.0)]:
        calculate(sphere, rext)
