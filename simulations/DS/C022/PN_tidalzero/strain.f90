!get the strain from Psi4
!directly from the waveforms by integration
!using as initial conditions what the PN
!strain should be
!notice also it assumes at this stage 
!psi4 is provided on a uniform time sequence

!notice we have harcoded the values of Real(h) and im(h)
!at the initial time

	 program main

	 real*8:: t(3), p4(3), h(3), psi4, Ipsi4
	 real*8:: Ip4(3), Ih(3)
	 integer :: j
	 real*8 :: hpsifactor
	 

!	 open (unit = 7, file = "sfho_PNish.dat")
!	 open (unit = 7, file = "dd2_PNish.dat")
	 open (unit = 7, file = "sfho_redux.dat")
!	 open (unit = 7, file = "dd2_redux.dat")

	 h = 0
	 p4 = 0
	
!this factor is 2 \sqrt(Pi/5) that comes from
!the 2Y22 decomposition

	hpsifactor= 1.585330754

	 do i=1, 100000

	 read (7,*,end=10) t1, psi4, Ipsi4

	if (i.eq.2) then
	  p4(1) = psi4
	  t(1) = t1
	  h(1) = 0.057189400503*hpsifactor

	  Ip4(1) = Ipsi4
	  Ih(1) = -0.000579427615*hpsifactor


	else if (i.eq.3) then
	  p4(2) = p4(1) 
	  p4(1) = psi4
	  t(2) = t(1)
	  t(1) = t1
	
	  h(2) = h(1)
	  h(1) =  0.057181331517*hpsifactor

	  Ip4(2) = Ip4(1) 
	  Ip4(1) = Ipsi4

	  Ih(2) = Ih(1)
	  Ih(1) =  -0.001158821878*hpsifactor


	else if (i.ge.4) then
	  p4(3) = p4(2) 
	  p4(2) = p4(1) 
	  p4(1) = psi4
	  t(3) = t(2)
	  t(2) = t(1)
	  t(1) = t1

	  h(3) = h(2)
	  h(2) = h(1)
	  dt = t(1) - t(2)
	  h(1) = dt**2*p4(2) +2.0d0*h(2) - h(3)


	  Ip4(3) = Ip4(2) 
	  Ip4(2) = Ip4(1) 
	  Ip4(1) = Ipsi4


	  Ih(3) = Ih(2)
	  Ih(2) = Ih(1)
	  dt = t(1) - t(2)
	  Ih(1) = dt**2*Ip4(2) +2.0d0*Ih(2) - Ih(3)


	
	end if  

	 if(i.ge.3) then
	 write(15,*) t(1), h(1), Ih(1)
	 end if

	end do

10 	continue
	end
