#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *




def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')



c1=[0.06,0.12,0.18,0.22,0.28]
freq=[0.0141176,0.0227124,0.0313181,0.0371569,0.0470588]
freq2=[0.0528431,0.0812745,0.101373,0.10701,0.112892]

freq  = 2*np.pi*np.array(freq)
freq2 = 2*np.pi*np.array(freq2)

cbin   = [0.9/8.9,0.98/4.6,1.07/2.5,0.5]
freqbin =[ 0.117, 0.203, 0.329,0.5]

m,b = np.polyfit(c1, freq, 1)
#m2,b2 = np.polyfit(c1, freq2, 1)
print m
print b

freq3=[m*0.06+b,m*0.12+b,m*0.18+b,m*0.22+b,m*0.28+b]
#freq4=[m2*0.06+b2,m2*0.12+b2,m2*0.18+b2,m2*0.22+b2,m2*0.28+b2]

#freq006 = [0.0141176]

# Two subplots not sharing x axes
#f, axarr = pl.subplots(1, sharex=False)

#pl.figure(figsize=(8,8))

pl.ylim(0.0, 0.75)
pl.xlim(0.0, 0.6)

##ax.set_yscale('log')
#pl.semilogy()

figure(1)
pl.plot(c1,freq3, 'r--')
#pl.plot(c1[0], freq[0], 'r',linestyle='_',marker='o',markersize=10, label=r'$C=0.06$'  )
pl.plot(c1[0], freq[0], 'r',marker='o',markersize=10, label=r'$C=0.06$'  )
pl.plot(c1[0], freq2[0], 'r',linestyle=':',marker='o',markersize=10,  )
pl.plot(c1[1], freq[1], 'b',linestyle=':',marker='o',markersize=10, label=r'$C=0.12$' )
pl.plot(c1[1], freq2[1], 'b',linestyle=':',marker='o',markersize=10 )
pl.plot(c1[2], freq[2], 'k',linestyle=':',marker='o',markersize=10, label=r'$C=0.18$' )
pl.plot(c1[2], freq2[2], 'k',linestyle=':',marker='o',markersize=10 )
pl.plot(c1[3], freq[3], 'g',linestyle=':',marker='o',markersize=10, label=r'$C=0.22$' )
pl.plot(c1[3], freq2[3], 'g',linestyle=':',marker='o',markersize=10 )
pl.plot(c1[4], freq[4], 'm',linestyle=':',marker='o',markersize=10, label=r'$C=0.28$' )
pl.plot(c1[4], freq2[4], 'm',linestyle=':',marker='o',markersize=10 )

#pl.plot(c1, freq2, 'b',linestyle='_',marker='o',markersize=10)
pl.plot(cbin, freqbin, 'c',linestyle=':',marker='s',markersize=10)
#x   = np.arange(0.0, 0.25, 0.01)
#ybs = 0.0665584 + 2.1906*x
#yns =-0.0408803 + 2.25395*x

#pl.plot(x, ybs, 'k',linestyle='--',linewidth=2)
#pl.plot(x, yns, 'r',linestyle=':',linewidth=2)


#pl.plot(fc2, fp2, 'k',label='BS')
#pl.plot(fc1, fp1, 'r',label='NS')

pl.legend(loc='lower right', labelspacing=0.2,prop={'size':19},ncol=1)
#pl.legend(loc='lower right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$C$',fontsize=20)
pl.ylabel(r'$M \omega$',fontsize=20)
#pl.xlim(0.004, 0.15)
#pl.ylim(2E-6, 5E-1)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
pl.xticks(fontsize=16)
pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
savefig('freqvscomp.pdf')





