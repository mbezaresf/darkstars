#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

dataset = np.genfromtxt(fname='a/DFT_a006.dat')
t1=dataset[:,0]
x1=dataset[:,1]


dataset = np.genfromtxt(fname='a/DFT_a012.dat')
t2=dataset[:,0]
x2=dataset[:,1] 

dataset = np.genfromtxt(fname='a/DFT_a018.dat')
t3=dataset[:,0]
x3=dataset[:,1] 

dataset = np.genfromtxt(fname='a/DFT_a022.dat')
t4=dataset[:,0]
x4=dataset[:,1]

dataset = np.genfromtxt(fname='b/DFT_b028.dat')
t5=dataset[:,0]
x5=dataset[:,1]


# Two subplots not sharing x axes
#f, axarr = pl.subplots(1, sharex=False)

#pl.figure(figsize=(8,8))

#pl.ylim(-0.6, 0.1)

##ax.set_yscale('log')
pl.semilogy()

figure(1)
pl.plot(t1, x1, 'r',linewidth = 4,linestyle='-',label=r'$C=0.06$')
pl.plot(t2, x2, 'b',linewidth = 4,linestyle='-',label=r'$C=0.12$')
pl.plot(t3, x3, 'k',linewidth = 4,linestyle='-',label=r'$C=0.18$')
pl.plot(t4, x4, 'g',linewidth = 4,linestyle='-',label=r'$C=0.22$')
#pl.plot(t5, x5, 'm',linewidth = 4,linestyle='-',label=r'$C=0.28$')
pl.legend(loc='lower right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$f$',fontsize=20)
pl.ylabel(r'${\tilde h}^{2,2}$',fontsize=20)
pl.xlim(0.005, 0.15)
pl.ylim(5E-9, 2E-4)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#pl.xticks(fontsize=16)
#pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
savefig('qnm_isolated_a.pdf')


#figure(2)
#pl.plot(t1, n1, 'r',linewidth = 2,label=r'$C=0.06$')
#pl.plot(t2, n2, 'b',linewidth = 2,label=r'$C=0.12$')
#pl.plot(t3, n3, 'k',linewidth = 2,label=r'$C=0.18$')
#pl.plot(t4, n4, 'g',linewidth = 2,label=r'$C=0.22$')
#pl.legend(loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.xlabel(r'$t$',fontsize=20)
#pl.ylabel(r'$||\psi_{4}||$',fontsize=20)
#pl.xlim(00.0, 1800.0)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#pl.xticks(fontsize=16)
#pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
#savefig('norm_gw_all_A.pdf')



