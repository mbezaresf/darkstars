###############################################################
#  Number of iterations
#nt0                  := 0
nt0                  := 70

#  Number of gridpoints for base grid
nx0                  := 133
ny0                  := 133
nz0                  := 133

#  Coordinate ranges for base grid
minx0                := -264.0
maxx0                :=  264.0
miny0                := -264.0
maxy0                :=  264.0
minz0                := -264.0
maxz0                :=  264.0

#  Courant factor
lambda               :=  0.20

################################################################
#  Output parameters
###############################################################
# output_style = 1 -> separate
#              = 2 -> together
#              = 3 -> both
#              = 4 -> ?
#              = 5 -> 1-D
#              = 6 -> both
output_style      :=  4
output_dim        := 3

#
# Dimension of SDF output:
#   (dimensionally reduced corresponds to slices through
#    the middle of the coarse grid):
#
#    [1,2,3] --> select dimension to be output
#    [4,5,6] --> sum the dimensions to be output if more than one
#    7       --> for output of [1 and 2] dimensional SDFs
#

#     Output types:
#          3---3D output over the region (lb1,lb2,lb3)-->(ub1,ub2,ub3)
#         11---1D output along x with constant y=lb2 and z=lb3
#         12---1D output along y with constant x=lb1 and z=lb3
#         13---1D output along z with constant x=lb1 and y=lb2
#         21---2D output with constant x=lb1
#         22---2D output with constant y=lb2
#         23---2D output with constant z=lb3


# Family 1:
output_f1_type   := 21
output_f1_level  := 0
output_f1_period := 0
output_f1_lb1    := 0.0
output_f1_lb2    :=-240.0
output_f1_lb3    :=-240.0
output_f1_ub1    := 0.0
output_f1_ub2    := 240.0
output_f1_ub3    := 240.0
# Family 2:
output_f2_type   := 22
output_f2_level  := 0
output_f2_period := 0
output_f2_lb1    :=-240.0
output_f2_lb2    := 0.0
output_f2_lb3    :=-240.0
output_f2_ub1    := 240.0
output_f2_ub2    := 0.0
output_f2_ub3    := 240.0
# Family 3:
output_f3_type   := 23
output_f3_level  := 0
output_f3_period := 5
output_f3_lb1    :=-240.0
output_f3_lb2    :=-240.0
output_f3_lb3    := 0.0
output_f3_ub1    := 240.0
output_f3_ub2    := 240.0
output_f3_ub3    := 0.0
# Family 4:
output_f4_type    := 3
output_f4_level   := 0
output_f4_period  := 0
output_f4_lb1     :=-160.0
output_f4_lb2     :=-160.0
output_f4_lb3     :=-160.0
output_f4_ub1     := 160.0
output_f4_ub2     := 160.0
output_f4_ub3     := 160.0


# variables to output

output_phiR   := 1110
output_phiI   := 1110
output_piR    := 0
output_piI    := 0


output_gt11   := 0
output_gt12   := 0
output_gt13   := 0
output_gt22   := 0
output_gt23   := 0
output_gt33   := 0
output_A11    := 0
output_A12    := 0
output_A13    := 0
output_A22    := 0
output_A23    := 0
output_A33    := 0
output_chi    := 11111
output_trK    := 11111

output_Gam1    := 0
output_Gam2    := 0
output_Gam3    := 0

output_alpha  := 11111
output_shift1 := 0
output_shift2 := 0
output_shift3 := 0

output_gb1    := 0
output_gb2    := 0
output_gb3    := 0

output_ham  := 1110
output_momx := 1110
output_momy := 1110
output_momz := 1110

output_tr_A     := 1110
output_detgt_m1 := 1110
output_Zx       := 1110
output_Zy       := 1110
output_Zz       := 1110
ouptut_theta    := 1110

output_massADM   := 1110
output_JzADM     := 1110
output_massKomar := 11111
output_JzKomar   := 11111
output_noether   := 11111

#
# analysis_XXX:  Perform some analysis on the given field XXX as per:
#          NB1:  Experimental so far, nothing done yet
#          NB2:  Computed each time step on the
#                      effective coarse level (0 if no shadow, 1 otherwise)
#          NB3:  For more than one, simply sum up desired options
#         == 0  [default] no analysis
#         == 1  spatial integral
#         == 2  l2norm
#         == 4  global min
#         == 8  global max
#         ==16  global abs min
#         ==32  global abs max
#         ==64  first and second moments (com and quadrupole)
#
#
analysis_ham   := 2
analysis_momx  := 2
analysis_momy  := 2
analysis_momz  := 2

analysis_tr_A     := 2
analysis_detgt_m1 := 2
analysis_Zx       := 2
analysis_Zy       := 2
analysis_Zz       := 2
analysis_theta    := 2

analysis_massKomar  := 1
analysis_JzKomar    := 1
analysis_noether    := 1

output_psi4R   := 11111
output_psi4I   := 11111

#
#
output_chr   := 0
output_error := 0

chkpt_period     := 100000
chkpt_readstate  := 0
chkpt_control    := 1

tag                  :=  '   '

output_level_0       :=  0
output_level_1       :=  0
output_level_2       :=  0
output_level_3       :=  0
output_level_4       :=  0
output_level_5       :=  0
output_level_6       :=  0
output_level_7       :=  0
output_level_8       :=  0
output_level_9       :=  0
output_level_10      :=  0


###############################################################
#
#  Method parameters
#
###############################################################
runge_kutta_type     := 2
initial_analysis     := 1
weno_interp          := 0
linearbounds         := 0
damp                 := 0

# evolve geometry and em fields
evolve_geometry      := 1
evolve_scalar_field  := 1
evolve_em_field      := 0

# It appears the variable to use is instead force_free
#   though it looks like electrovac isn't completely
#   implemented since the current isn't set to zero
#   in include/bssn_emtest.h
# evolve electrovacuum (0) or force-free (1)
force_free           := 0
enforce_bssn         := 0

# parameters of the scalar field potential
#  V = mass**2*phi^2*(1 - phi^2/sigma^2)^2
#sf_mass    := 1.0
#sf_sigma   := 1.0e10

sf_mass    := 8.5663999731631240
sf_sigma   := 0.05

###############################################################
#
#  Initial Data parameters
#
###############################################################

#  idtype   = 1 -> Shocktube
#           = 2 ->
idtype               := 0
gr_idtype            := 0
mhd_idtype           := 5

# geometry_type = 0 binary black hole
#               = 10 single boson star                  
#                    11 binary boson star boosted
#                    12 binary boson star phase shift
geometry_type        := 10

boundary_conditions  := 7
gh_bound_cond        := 7
CPBC_type            := 2
fluid_bound_cond     := 5

constraints_analysis := 1
psi4_analysis        := 0

# black holes
bssn_lambda_0        := 1
bssn_lambda_1        := 1
bssn_lambda_2        := 1
bssn_lambda_3        := 1
bssn_lambda_4        := 1
bssn_lambda_f0       := 1.0
bssn_lambda_f1       := 0.0
bssn_lambda_f2       := 1.0
bssn_lambda_f3       := 0.0
bssn_eta             := 1.0
bssn_trk0            := 0.0

bssn_kappa_z1        := 0.1
bssn_kappa_z2        := 0.0
bssn_kappa_cong      := 1.0
bssn_kappa_conA      := 1.0

# boson star
#bssn_lambda_1        := 0
#bssn_lambda_2        := 0
#bssn_lambda_3        := 0
#bssn_lambda_4        := 0
#bssn_lambda_f0       := 0.0
#bssn_lambda_f1       := 0.0
#bssn_lambda_f2       := 0.0
#bssn_lambda_f3       := 0.5
#bssn_eta             := 0.0
#bssn_trk0            := 0.0


bssn_matter_derivs   := 0
bssn_adv_derivs      := 1

bssn_eta_damping     := 1
bssn_eta_damping_exp := 2.0
bssn_R_0             := 20.0

# New matter parameters:
#  Electric Monopole (Reisner-Nordstrom in isotropic coordinates)
initial_b            := 2
bamp                 := 0.0
e1_amp               := 0.0

#  Boson star parameters
#  for QCO, y=4 and vx=0.2
bs_1_x0              := 0.0
bs_1_y0              :=-5.0
bs_1_z0              := 0.0
bs_1_vx              :=-0.14
#bs_1_vx              :=-0.22
bs_1_vy              := 0.0
bs_1_vz              := 0.0
bs_1_omega           := 1.7566921503056392

bs_2_x0              := 0.0
bs_2_y0              :=+5.0
bs_2_z0              := 0.0
bs_2_vx              :=+0.14
#bs_2_vx              :=+0.22
bs_2_vy              := 0.0
bs_2_vz              := 0.0
bs_2_omega           := 1.7566921503056392

bs_2_eps             := 1.0
bs_2_desf            := 0.0

# Old black hole parameters:
bh_n                 := 2
bh_type              := 1

bh_1_mass            :=  0.0
bh_1_x               :=  0.0
bh_1_y               :=  0.0
# give a small offset in the z-direction to help keep the
# black holes off of grid points...
bh_1_z               :=  0.00123
bh_1_px              := 0.0
bh_1_py              := 0.0
bh_1_pz              := 0.0
# note that a = spin/mass**2...
bh_1_spin            :=  0.0
bh_1_sth             :=  0.0
bh_1_sphi            :=  0.0


# surface extraction

asf_level            := 2
asf_period           := 1
asf_rconst           := 40.0
asf_ntheta           := 161
asf_nphi             := 81

bsf_level            := 2
bsf_period           := 1
bsf_rconst           := 50.0
bsf_ntheta           := 161
bsf_nphi             := 81

csf_level            := 2
csf_period           := 1
csf_rconst           := 60.0
csf_ntheta           := 161
csf_nphi             := 81

asf_out_rap4         := 2
asf_out_iap4         := 2
asf_out_amass        := 2
asf_out_aJz          := 2

bsf_out_rbp4         := 2
bsf_out_ibp4         := 2
bsf_out_bmass        := 2
bsf_out_bJz          := 2

csf_out_rcp4         := 2
csf_out_icp4         := 2
csf_out_cmass        := 2
csf_out_cJz          := 2


#
# These parameters are used to terminate code
# execution when the max of the abs value of the
# field reaches some threshold after some minimum
# amount of time:
#     (NB: if maxchi_thresh==0, then this feature
#          is turned off                         )
#
maxchi_thresh        :=   0.0
maxchi_minctime      :=   0.0

###############################################################
#
#  parameters
#
###############################################################
#  dissipation:  0 -> No artificial dissipation
dissipation      := 42
sigma_diss       := 0.16
deriv_order      := 42
extradiss        := 0.0
extradissout     := 0.0


###############################################################
#
#  Boundary parameters
#
###############################################################

#   WARNING: bc_type may not actually do anything
#   bc_type = 0 -> Same BC applied to all functions
#           = 1 -> Group functions
bc_type              :=  0
#   inner_bound_data = 0 -> DO NOT Supply boundary data at inner exision surface
#                    = 1 -> Supply boundary data at inner exision surface
inner_bound_data     :=  0
#  Set how boundary conditions are applied to the metric.
#  Not applicable here?  Leave at default value.

##############################################################
#
#  AMR parameters
#
#       For simple FMR, set refine_period very high,
#       and set simpleFMR to some value greater than 0
#       where the value is inversely related to the volume
#       resolved (simpleFMR==8 is a good value).
#
#       ClusterDD controls whether grids get decomposed:
#         0 (default) always try to decompose
#         1           don't decompose coarse grid
#         >1          never decompose
#
#       Clusterstyle restricts the clustering:
#         0 (default) normal clustering
#         1           no cuts based on inflection points
#
###############################################################
#  Maximum number of refinement levels, must be less than 25.
allowedl             :=  7
#allowedl             :=  4
refine_factor        :=  2
refine_period        :=  32
simpleFMR            :=  0
clusterstyle         :=  1
diss_afterinj        :=  0
trace_level          :=  3
#ethreshold           :=  0.4
ethreshold           :=  0.00002
G_scale_factor       :=  1.0
shadow               :=  0
buffer               :=  6
mindim               :=  6
window               :=  4
minefficiency        :=  0.5


refine_level_0 = -2.2
refine_level_1 = -1.5
refine_level_2 = -1.2
#refine_level_3 = -4.2
#refine_level_4 = -1.5
#refine_level_4 = -2.0
#refine_level_5 = -2.0
#refine_level_6 = -2.0


#
# For ICN, you probably just need this to be 1
# For RK3, you'll probably want this to be 6
#    Related to this, you need to check the flags MOLboundaries and ICN
#    in the code. At some point, these flags will get converted into
#    parameters one would set here.
#
ghostwidth           :=  7
bound_width          :=  5

#
# Choice the update scheme (see had/src/hyper/rk3.f90)
#    0 ==> RK3
#    1 ==> ICN
#
update_scheme        := 0
#
# Regarding setting the boundary values of AMR subgrids:
#    set    *before* the evolution step ===> 0
#        or *after*                     ===> 1 ?
#
amrbound_prepost     := 1
#
#    set    *only* when parent/child grids aligned ===> 0
#        or        at every time step of subgrid   ===> 1
#
amrbound_timealign   := 0

#
# clusterreadwrite --- controls reading cluster info from file (.cluster):
#        0 --- normal, dynamic refinement
#        1 --- read  in  hierarchy info from previously written file
#        2 --- write out hierarchy info
#
clusterreadwrite     :=  0

#
# For typical ICN, one would want:
#
#     update_scheme        := 1
#     amrbound_prepost     := 0
#     amrbound_timealign   := 1
#     ghostwidth           := 1
#
# For typical RK3, one would want:
#
#     update_scheme        := 0
#     amrbound_prepost     := 1
#     amrbound_timealign   := 0
#     ghostwidth           := 6
#

elliptic_solve             :=  -1
nvcycle                    :=  3
preswp                     :=  3
pstswp                     :=  3
maxsweeps                  := 5000
ell_epsilon                :=  5.0e-8
output_uell     := 111111
output_uell_st1 := 111111

