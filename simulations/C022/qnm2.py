#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d
from lmfit import minimize, Parameters
from numpy import sin




# I had to install lmfit but it was easy: 
#    http://lmfit.github.io/lmfit-py/intro.html

# This needs to be customized to the problem:
def residual(params, t, data, eps_data):
    amp    = params['amp']
    pshift = params['phase']
    freq   = params['frequency']
    decay  = params['decay']

   
    #model = amp * sin(t * freq  + pshift) * exp(t*decay)
    model = np.float64(amp * sin(t * freq  + pshift) * exp((t-301.3)*decay))
    #model = (amp * sin(t * freq  + pshift) * exp(t*decay)).astype(float)
    #model = (amp * sin(t * freq  + pshift) * exp(-t*decay)).astype(np.float64)

    return (data-model)
    #return (data-model)/eps_data



# number of points from end and after max to do analysis:
buffer = 50

figure()

dataset = np.genfromtxt(fname='./c2p2B.dat')
t1=dataset[:,0]
r1=dataset[:,1] 
i1=dataset[:,2] 
#----------------
tU = np.linspace(t1[0],max(t1),2000)
jR = interp1d(t1,r1)
jI = interp1d(t1,i1)
xU = jR(tU)
yU = jI(tU)
zU=xU+1j*yU
lmagU=log(sqrt(xU**2+yU**2))
atmax=argmax(lmagU)
# Restrict arrays to just the ringdown part:
#    (use some cusion amount)
ringt=tU[atmax+buffer:tU.size-buffer]
ringx=xU[atmax+buffer:tU.size-buffer]
ringy=yU[atmax+buffer:tU.size-buffer]
ringz=zU[atmax+buffer:tU.size-buffer]
ringm=lmagU[atmax+buffer:tU.size-buffer]

# carry out least square fit to the magnitude (linear fit):
fitp = polyfit(ringt,ringm,1)
fitm = poly1d(fitp)
pl.plot(ringt,ringm,'m',linewidth=3,label='magnitude')
pl.plot(ringt, fitm(ringt),'c',linewidth=3,label='fit')
plt.text(350,-10, 'y-intercept=%s \n slope=%s\n buffer= %s\n' % (fitm[0],fitm[1],buffer))

pl.legend(loc='upper right',labelspacing=0.2,prop={'size':15},ncol=1)
pl.xlabel('t',fontsize=20)
pl.ylabel(r'$|\Psi_4|$',fontsize=20)
pl.savefig("qnm.pdf")
pl.close()

fringz=fft(ringz)
psd=abs(fringz)**2
timestep=ringt[1]-ringt[0]
N=ringt.size
freqs=fftfreq(N,d=timestep)
idx=np.argsort(freqs)
# Look for max by fitting a polynomial
fitps  = polyfit(freqs,psd,4)
#fitps  = polyfit(freqs[idx],psd[idx],4)
fitpsd = poly1d(fitps)
# Plot everything:
pl.plot(freqs[idx],   psd[idx],label='numerical data')
pl.plot(freqs[idx],fitpsd(freqs[idx]),label='fit')
pl.legend(loc='upper right',labelspacing=0.2,prop={'size':15},ncol=1)
plt.text(0.12,5,' fitps[0]=%s \n fitps[1]=%s\n fitps[2]= %s\n fitps[3]=%s\n' % (fitps[0],fitps[1],fitps[2],fitps[3]))
pl.xlim([0,0.2])
pl.xlabel(r'$\omega$',fontsize=20)
pl.ylabel(r'PSD$|\Psi_4(\omega)|$',fontsize=20)
pl.savefig("qnmfft.pdf")

# Doing the lmfit:

params = Parameters()
params.add('amp', value=0.042,min=0.041,max=0.043)
params.add('decay', value=-0.059,min=-0.059,max=-0.057)
params.add('phase', value=2.8,min=1.5,max=4.5)
params.add('frequency', value=0.35,min=0.35,max=0.36)

# Just set this to 1 (with the same dimension as ringt
eps_data = 0.0*ringt+1.0
out = minimize(residual, params, args=(ringt, ringx, eps_data))
#out = minimize(residual, params, args=(ringt, ringz.real, eps_data))
