#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d
from numpy import sin
from scipy.optimize import curve_fit

def func(t, amp, freq, pshift, decay):
        return amp * sin(t * freq  + pshift) * exp(-1.0*(t-301.288644322)*decay)


# number of points from end and after max to do analysis:
buffer = 50

figure()

dataset = np.genfromtxt(fname='./c2p2B.dat')
t1=dataset[:,0]
r1=dataset[:,1] 
i1=dataset[:,2] 
#----------------
tU = np.linspace(t1[0],max(t1),2000)
jR = interp1d(t1,r1)
jI = interp1d(t1,i1)
xU = jR(tU)
yU = jI(tU)
zU=xU+1j*yU
lmagU=log(sqrt(xU**2+yU**2))
atmax=argmax(lmagU)
# Restrict arrays to just the ringdown part:
#    (use some cusion amount)
ringt=tU[atmax+buffer:tU.size-buffer]
ringx=xU[atmax+buffer:tU.size-buffer]
ringy=yU[atmax+buffer:tU.size-buffer]
ringz=zU[atmax+buffer:tU.size-buffer]
ringm=lmagU[atmax+buffer:tU.size-buffer]
begint=ringt[0]

# carry out least square fit to the magnitude (linear fit):
fitp = polyfit(ringt,ringm,1)
fitm = poly1d(fitp)
pl.plot(ringt,ringm,'m',linewidth=3,label='magnitude')
pl.plot(ringt, fitm(ringt),'c',linewidth=3,label='fit')
plt.text(350,-10, 'y-intercept=%s \n slope=%s\n buffer= %s\n' % (fitm[0],fitm[1],buffer))
decayrate = fitm[1]

pl.legend(loc='upper right',labelspacing=0.2,prop={'size':15},ncol=1)
pl.xlabel('t',fontsize=20)
pl.ylabel(r'$|\Psi_4|$',fontsize=20)
pl.savefig("qnm.pdf")
pl.close()

fringz=fft(ringz)
psd=abs(fringz)**2
timestep=ringt[1]-ringt[0]
N=ringt.size
freqs=fftfreq(N,d=timestep)
idx=np.argsort(freqs)
# Look for max by fitting a polynomial
fitps  = polyfit(freqs,psd,4)
#fitps  = polyfit(freqs[idx],psd[idx],4)
fitpsd = poly1d(fitps)
# Can't get fit working and so just take max of data:
atmaxi=argmax(psd)
freqmax=freqs[atmaxi]
# Plot everything:
pl.plot(freqs[idx],   psd[idx],label='numerical data')
pl.plot(freqs[idx],fitpsd(freqs[idx]),label='fit')
pl.legend(loc='upper right',labelspacing=0.2,prop={'size':15},ncol=1)
plt.text(0.12,5,' freq at max=%s\n' % (freqmax))
#plt.text(0.12,5,' fitps[0]=%s \n fitps[1]=%s\n fitps[2]= %s\n fitps[3]=%s\n' % (fitps[0],fitps[1],fitps[2],fitps[3]))
pl.xlim([0,0.2])
pl.xlabel(r'$\omega$',fontsize=20)
pl.ylabel(r'PSD$|\Psi_4(\omega)|$',fontsize=20)
pl.savefig("qnmfft.pdf")
pl.close()


# Now reconstruct total fit and compare to data:
#totalfit = np.vectorize(ringt)
#totalfit = 0.2*sin(ringt)
#totalfit = 0.02*math.sin(0.057*ringt)*exp(decayrate*ringt)
#for i in range(len(ringt)):
#   totalfit = 0.02*math.sin(freqmax*ringt[i])*exp(decayrate*ringt[i])
#print len(ringt)
#print len(ringx)
#print len(totalfit)
print begint
totalfit = 0.042*sin(6.28*freqmax*ringt+0.9*math.pi)*exp(decayrate*(ringt-begint))
#totalfit = 0.02*cos(freqmax*ringt)*exp(decayrate*ringt)
#
pl.plot(ringt, ringx,     label='numerical data')
pl.plot(ringt, totalfit,  label='total fit')
plt.text(310,-0.02, '0.042*sin(t* %s + %s)*exp( (t-%s)*%s) \n' % (round(6.28*freqmax,4),round(0.9*pi,4),round(begint,4),round(decayrate,4)),color="green")
# use curve_fit:
#popt, pcov = curve_fit(func, ringt, ringx)
popt, pcov = curve_fit(func, ringt, ringx, bounds=(0,[0.07,0.8,119.28,0.1]))
plt.plot(ringt, func(ringt, *popt), 'r-', label='curve fit')
print popt
plt.text(310,-0.03, '%s *sin(t* %s + %s)*exp( (t-301.3)*-%s) \n' % (round(popt[0],4),round(popt[1],4),round(popt[2],4),round(popt[3],4)),color="red")
pl.legend(loc='upper right',labelspacing=0.2,prop={'size':15},ncol=1)
pl.xlabel('t',fontsize=20)
pl.ylabel(r'$|\Psi_4|$',fontsize=20)
pl.savefig("qnmtotalfit.pdf")
pl.close()


