! the NR data has repeated times and
! also non uniform distribution
! this is a hack to get things to have the same spacing
! as in the PN stuff. Notice there is lots of hardcoding!
! in particular that we do a linear interpolation
! and that the spacing is giving by dtspace=1 (in time)

	program main	
	implicit none
	real*8 :: rp4(1:100000), ip4(1:100000), t1(1:100000) 
	integer :: imax, i, j
	real*8 :: dtspace, tout, rp4out, ip4out
	 open (unit = 8, file = "nrsfho.dat")	

	dtspace = 1.
	j =0
	 do i=1, 100000
	 j = j+1
	 read (8,*,end=10) t1(j), rp4(j), ip4(j)
	  if(j.gt.1.and.t1(j).eq.t1(j-1)) j=j-1
	end do
10 	continue
	 imax = j - 1

!ok, now will try to get something come out uniform
!with a spacing of dt
	 write(13,*) t1(1), rp4(1), ip4(1)

	do j = 1, 10000
	 if(tout.gt.t1(imax)) exit
	tout = j*dtspace

	  do i=1, imax
	   if(t1(i).gt.tout) exit
	  end do

	   if(i.le.2000000.or.i.ge.imax-2) then
	   rp4out = rp4(i-1)*(tout-t1(i))/(t1(i-1)-t1(i)) + &
     &              rp4(i)*(tout-t1(i-1))/(t1(i)-t1(i-1))

	 
	   ip4out = ip4(i-1)*(tout-t1(i))/(t1(i-1)-t1(i)) + &
     &              ip4(i)*(tout-t1(i-1))/(t1(i)-t1(i-1))

	   else

	   rp4out = rp4(i-2)*(tout-   t1(i-1))*(tout-   t1(i))*(tout-   t1(i+1)) &
     &                      /(t1(i-2)-t1(i-1))*(t1(i-2)-t1(i))*(t1(i-2)-t1(i+1)) &
     &	          + rp4(i-1)*(tout-   t1(i-2))*(tout-   t1(i))*(tout-   t1(i+1)) &
     &                      /(t1(i-1)-t1(i-2))*(t1(i-1)-t1(i))*(t1(i-1)-t1(i+1)) &
     &	          + rp4(i)*(tout-t1(i-2))*(tout- t1(i-1))*(tout-   t1(i+1)) &
     &                   /(t1(i)-t1(i-2))*(t1(i)-t1(i-1))*(t1(i-1)-t1(i+1)) &
     &	          + rp4(i+1)*(tout-   t1(i-2))*(tout-   t1(i-1))*(tout-   t1(i)) &
     &                      /(t1(i+1)-t1(i-2))*(t1(i+1)-t1(i-1))*(t1(i+1)-t1(i)) 

	   ip4out = ip4(i-2)*(tout-t1(i-1))*(tout-t1(i))*(tout-t1(i+1)) &
     &                      /(t1(i-2)-t1(i-1))*(t1(i-2)-t1(i))*(t1(i-2)-t1(i+1)) &
     &	          + ip4(i-1)*(tout-t1(i-2))*(tout-t1(i))*(tout-t1(i+1)) &
     &                      /(t1(i-1)-t1(i-2))*(t1(i-1)-t1(i))*(t1(i-1)-t1(i+1)) &
     &	          + ip4(i)*(tout-t1(i-2))*(tout-t1(i-1))*(tout-t1(i+1)) &
     &                   /(t1(i)-t1(i-2))*(t1(i)-t1(i-1))*(t1(i-1)-t1(i+1)) &
     &	          + ip4(i+1)*(tout-t1(i-2))*(tout-t1(i-1))*(tout-t1(i)) &
     &                      /(t1(i+1)-t1(i-2))*(t1(i+1)-t1(i-1))*(t1(i+1)-t1(i)) 

	   end if
	 write(13,*) tout, rp4out, ip4out

	end do


	end
