We thank the Referee for both the favourable recommendation and the suggestions for improvements.

General points

# It should be mentioned in the abstract and introduction as well that the authors only consider equal mass cases for their binary simulations.
In other parts of the manuscript, they use the term “identical stars”, but “equal mass stars” is more appropriate, 
because they are actually thought to be made of different scalar fields. Please change this throughout the text.

[M]

# On page 5 they write "contact time tc (i.e., defined as the time at which the individual Noether charge densities make contact for the first time)", but what is the exact definition of "making contact for the first time"? If it is defined through R_N, than it should be explicitly written here.

# Why do the authors not show data for all models in figure 5?

# In Section IIC, the authors mention the blobs that were observed in previous simulations of their own. The authors should comment on how the dynamics of such blobs change with resolution and grid setup.

Also, referring to the BS C=0.12 "blob" case, the authors say that "The evolution of angular momentum in Fig. 5 also shows considerable losses in the BS case (i.e., resulting into a sudden decrease after the contact time) as a result of the matter ejection." To me, however, the curve for BS C=0.12 does not seem so different from that for BS C=0.06, which has no blobs. The authors should explain further or remove this sentence.

# In the same section, at line 47, the authors use the word "Therefore", which to me seems to indicate that the authors have understood from this very simulation that "one of the effects of matter interactions is to induce an additional pressure that supports the collapse to a BH". However I think that it is a well expected outcome, so the sentence should be rewritten for not giving the impression that this is something newly discovered from that simulation.

# In their conclusions, the authors state that "All these results indicates [sic] that there is a new kind of objects, the Dark Stars", but simulations cannot establish (or even "indicate") the existence of unobserved objects. Therefore, the authors should tone down their statement.

# Appendix A. The authors mention "Considering the same assumptions", but it is not clear what "same assumptions" they are. Not only those in the brackets following this phrase, since there are more assumptions in the reference articles they cite. It may be a good idea to write down such assumptions explicitly.
Also, for the convenience of the reader, the authors should write which equation of Ref. 54 is used to derive their formula for E_contact (the first formula in the appendix, without a number; a number should be added).

Minor points

- page 5, left, line 53: the words "(i.e., and," seem out of place; please change

- page 5, right, line 16 AND page 5, right, line 45 AND page 9, right, line 37:
change "low compact stars" to "low compactness stars"

- page 5, right, line 45: I find this sentence grammatically awkward:
"Angular momentum is radiated slowly during the coalescence through gravitational waves, increasing rapidly its emission rate after the contact time." It may be changed to "Angular momentum is radiated slowly during the coalescence through gravitational waves, and its emission rate increases rapidly after the contact time."

- page 5, right, line 48: I recommend to remove the clause "which means that the final multi-state BS has approximately the total initial mass of the binary system" because what it states is obvious and redundant.

-page 6, Table II: In "Characteristics of binary of DBS models.", the plural should be used, namely "binaries".

- page 8, caption of figure 5, line 24: "C=012" is missing a dot.

- page 8, left, line 44: the "to" following "yielding" should be removed.

-page 8, right, line 26: "suffer" is not an appropriate verb here; please change.

-page 8, right, line 27 AND page 9, left, line 45: in this journal it may be better to change "different than" to "different from".

- page 9, left, line 45:
change "high-compact cases" to "high-compactness cases"

- page 10, left, line 53: "show" should be "shows"

- page 11, left, line 55: "GWs on this" should be "GWs in this"

- page 11, right, line 47" "accelerates" should be "accelerate" 
