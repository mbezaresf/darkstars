#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')


#for the NS
# Mi*omega_c = 2 * C^(3/2) --> fc1
# f_f obtained from the table II of arXiv:1603.00501
# frequencies converted from Hz to geometrized units by 
#   f[] = f[1/s] * (1 s/ 1000ms) * (1ms / 200)
# and then multiplied by 2*pi*Mf, where Mf=2.7, to get Mf*omega_f 
fc1=[0.100,0.118,0.139]
fp1=[0.172,0.198,0.2926]

#for the SBS 
# Mi*omega_c = 2 * C^(3/2) --> fc2
# frequencies are already in f[], so we only need
#to multiply by 2*pi*Mf for the fp2 (QNM)
# Mf*omega_f are from the table II (omega_f=2*pi*f_f)
fc2=[0.0294,0.0831,0.1527]
fp2b=[0.117,0.203,0.329]


#for the DS 
# Mi*omega_c = 2 * C^(3/2) --> fc3
# frequencies are already in f[], so we only need
#to multiply by 2*pi*Mf for the fp3 (QNM)
fc3=[0.0294,0.0831]
# Mf*omega_f are from the table II (omega_f=2*pi*f_f)
fp3b=[1.06*(2.0*math.pi*0.0153),1.12*(2.0*math.pi*0.0345)]


#exit()
# Two subplots not sharing x axes
#f, axarr = pl.subplots(1, sharex=False)

#pl.figure(figsize=(8,8))

pl.ylim(0.002, 0.42)
pl.xlim(0.0, 0.18)

##ax.set_yscale('log')
#pl.semilogy()

figure(1)
##### SOLITONIC BS #########
#pl.plot(fc2, fp2, 'k',linestyle='_',marker='s',markersize=10,label='BS')
pl.plot(fc2[0], fp2b[0], 'k',linestyle='None',marker='s',markersize=10,label='SBS')
pl.plot(fc2[1], fp2b[1], 'k',linestyle='None',marker='s',markersize=10)
pl.plot(fc2[2], fp2b[2], 'k',linestyle='None',marker='s',markersize=10,markeredgewidth=2,markeredgecolor='k',
markerfacecolor='None')

##### DARK STARS #########
pl.plot(fc3[0], fp3b[0], 'b',linestyle='None',marker='s',markersize=10,label='DS')
pl.plot(fc3[1], fp3b[1], 'b',linestyle='None',marker='s',markersize=10)

##### NEUTRON STARS #########
pl.plot(fc1, fp1, 'r',linestyle='None',marker='o',markersize=10,label='NS')

a1,a0 = np.polyfit(fc2, fp2b, 1)
print "BS"
print a1
print a0

b1,b0 = np.polyfit(fc3, fp3b, 1)
print "DS"
print b1
print b0


x   = np.arange(0.01, 0.18, 0.001)
ybs = a0 + a1*x
yds = b0 + b1*x
yns =-0.136 + 2.96*x

pl.plot(x, ybs, 'k',linestyle='--',linewidth=2)
#pl.plot(x, yds, 'b',linestyle='-',linewidth=2)
pl.plot(x, yns, 'r',linestyle=':',linewidth=2)


#pl.plot(fc2, fp2, 'k',label='BS')
#pl.plot(fc1, fp1, 'r',label='NS')

pl.legend(loc='upper left', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$M_0 \omega_{c}$',fontsize=24)
pl.ylabel(r'$M_r \omega_r$',fontsize=24)
#pl.xlim(0.004, 0.15)
#pl.ylim(2E-6, 5E-1)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
pl.xticks(fontsize=16)
pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
savefig('omegas_all_ds.pdf')





