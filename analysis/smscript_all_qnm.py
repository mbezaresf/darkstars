#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

############# Solitonic Boson Stars #####################################
dataset = np.genfromtxt(fname='./strain_SBS/Fh2p2_inf_C006.dat')
t1=dataset[:,0]
x1=dataset[:,1]

dataset = np.genfromtxt(fname='./strain_SBS/Fh2p2_inf_C012.dat')
t2=dataset[:,0]
x2=dataset[:,1] 

dataset = np.genfromtxt(fname='./strain_SBS/Fh2p2_inf_C018new.dat')
t3=dataset[:,0]
x3=dataset[:,1] 

dataset = np.genfromtxt(fname='./strain_SBS/Fh2p2_inf_C022new.dat')
t4=dataset[:,0]
x4=dataset[:,1]

print "Max value element freq SBS: ", t1[x1.argmax()],t2[x2.argmax()],t3[x3.argmax()],t4[x4.argmax()]

############# Dark Stars Stars #####################################
dataset = np.genfromtxt(fname='./FT_DS/DS/FT_realh22/FT_h22_c006_ds')
t1_ds=dataset[:,0]
x1_ds=dataset[:,1]

dataset = np.genfromtxt(fname='./FT_DS/DS/FT_realh22/FT_h22_c012_ds')
t2_ds=dataset[:,0]
x2_ds=dataset[:,1] 

dataset = np.genfromtxt(fname='./FT_DS/DS/FT_realh22/FT_h22_c018_ds')
t3_ds=dataset[:,0]
x3_ds=dataset[:,1] 

dataset = np.genfromtxt(fname='./FT_DS/DS/FT_realh22/FT_h22_c022_ds')
t4_ds=dataset[:,0]
x4_ds=dataset[:,1]

print "Max value element freq : ", t2_ds[x2_ds.argmax()],t3_ds[x3_ds.argmax()],t4_ds[x4_ds.argmax()]


# Two subplots not sharing x axes
#f, axarr = pl.subplots(1, sharex=False)

#pl.figure(figsize=(8,8))

#pl.ylim(-0.6, 0.1)

##ax.set_yscale('log')
pl.semilogy()

figure(1)
pl.plot(t1, x1, 'r',linewidth = 3,linestyle=':',label=r'$C=0.06$ SBS')
pl.plot(t1_ds, x1_ds, 'r',linewidth = 1,linestyle='-',label=r'$C=0.06$ DBS')
pl.plot(t2, x2, 'b',linewidth = 3,linestyle=':',label=r'$C=0.12$ SBS')
pl.plot(t2_ds, x2_ds, 'b',linewidth = 1,linestyle='-',label=r'$C=0.12$ DBS')
pl.plot(t3, x3, 'k',linewidth = 3,linestyle=':',label=r'$C=0.18$ SBS')
pl.plot(t3_ds, x3_ds, 'k',linewidth = 1,linestyle='-',label=r'$C=0.18$ DBS')
pl.plot(t4, x4, 'g',linewidth = 3,linestyle=':',label=r'$C=0.22$ SBS')
pl.plot(t4_ds, x4_ds, 'g',linewidth = 1,linestyle='-',label=r'$C=0.22$ DBS')
pl.legend(loc='upper right', labelspacing=0.2,prop={'size':13},ncol=2,bbox_to_anchor=(1.1, 1.0))
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$f$',fontsize=22)
pl.ylabel(r'${\tilde h}^{2,2}(f)$',fontsize=22)
pl.xlim(0.004, 0.15)
pl.ylim(2E-6, 5E-1)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
pl.xticks(fontsize=14)
pl.yticks(fontsize=14)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
savefig('qnm_all_ds.pdf',  bbox_inches="tight")


#figure(2)
#pl.plot(t1, n1, 'r',linewidth = 2,label=r'$C=0.06$')
#pl.plot(t2, n2, 'b',linewidth = 2,label=r'$C=0.12$')
#pl.plot(t3, n3, 'k',linewidth = 2,label=r'$C=0.18$')
#pl.plot(t4, n4, 'g',linewidth = 2,label=r'$C=0.22$')
#pl.legend(loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.xlabel(r'$t$',fontsize=20)
#pl.ylabel(r'$||\psi_{4}||$',fontsize=20)
#pl.xlim(00.0, 1800.0)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#pl.xticks(fontsize=16)
#pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
#savefig('norm_gw_all_A.pdf')



