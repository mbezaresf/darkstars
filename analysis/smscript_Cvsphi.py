#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *

com=[0.06,0.12,0.18,0.22]

dataset = np.genfromtxt(fname='data.dat')
x1 = dataset[:,0]
x2 = dataset[:,1] 
x3 = dataset[:,2]
x4 = dataset[:,3]
x5 = dataset[:,4]
x6 = dataset[:,4]

pl.xlim(1.0, 1.1)
#pl.ylim(0.0, 0.18)

C = 1.0/x5

np.savetxt('phi_C.out', np.c_[x1,C] )

dataset = np.genfromtxt(fname='phi_C_mathplot.out')
t1 = dataset[:,0]
t2 = dataset[:,1] 

print t2[39],t2[42],t2[43],t2[44]

figure(1)
pl.plot(t1, t2, 'k-')
pl.plot(t1[39], t2[39], 'r',marker='o',markersize=10,linestyle='None',label=r'$C=0.06$' )
pl.plot(t1[42], t2[42], 'b',marker='o',markersize=10,linestyle='None',label=r'$C=0.12$' )
pl.plot(t1[43], t2[43], 'k',marker='o',markersize=10,linestyle='None',label=r'$C=0.18$' )
pl.plot(t1[44], t2[44], 'g',marker='o',markersize=10,linestyle='None',label=r'$C=0.22$' )
pl.legend(loc='upper left', labelspacing=0.4,prop={'size':20},ncol=1,numpoints=1)
#pl.legend(loc='best', shadow=True, fancybox=True, numpoints=1)
pl.xlabel(r'$\phi_{c}/\sigma_{0}$',fontsize=20)
pl.ylabel(r'$M/R$',fontsize=20)
pl.xlim(0.4, 1.8)
pl.ylim(0.0, 0.34)
pl.tick_params(axis='both', which='major', labelsize=15)
#pl.show()
savefig('phi_C.pdf',bbox_inches="tight")





