#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d

# compare to Carlos' signal:
dataset = np.genfromtxt(fname='../simulations/C006/h2p2_inf.dat')
ct06=dataset[:,0]
cx06=dataset[:,1] 
dataset = np.genfromtxt(fname='../simulations/C012/h2p2_inf.dat')
ct12=dataset[:,0]
cx12=dataset[:,1] 
dataset = np.genfromtxt(fname='../simulations/C018/h2p2_inf.dat')
ct18=dataset[:,0]
cx18=dataset[:,1] 
dataset = np.genfromtxt(fname='../simulations/C022/h2p2_inf.dat')
ct22=dataset[:,0]
cx22=dataset[:,1] 

# compare to EOB
dataset = np.genfromtxt(fname='../simulations/EOB/htSEOBNRv4_22.dat')
ct=dataset[:,0]
cx=dataset[:,1]
cy=dataset[:,2]

#t1=(datascet[:,0] - tm1)/M1
#x1=dataset[:,1]/M1 
#y1=dataset[:,2]/M1 


#had2ms = 0.004926420937225289
figure()
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')
##################################################
##     strain                                   ##
##################################################
def strain(t, x, y, cutoff):
   # Interpolate to uniform grid:
   tU     = np.linspace(t[0],max(t),len(t))
   jR     = interp1d(t,x)
   jI     = interp1d(t,y)
   xU     = jR(tU)
   yU     = jI(tU)
   zU     = xU+1j*yU
   # Compute the FFT:
   fftU   = fft(zU)
   tstep  = tU[1]-tU[0]
   N      = zU.size
   # Compute the frequencies of the FFT:
   fftU_f = fftfreq(N,d=tstep)
   # Kill the DC offset:
   fftU[0]= 0.0
   # Change the low-end frequencies
   freqs=fftU_f
   # Make sure zero frequency does not cause error
   freqs[0]=1.0
   for i in range(len(fftU_f)):
      if (abs(fftU_f[i]) < cutoff):
         freqs[i] = cutoff*sign(fftU_f[i])
      else:
         freqs[i] = fftU_f[i]
   # Do the integration and inverse FFT:
   strain = -ifft(fftU/((2.0*pi*freqs)**2))
   return tU, strain
#####################################################
############# SOLITONIC BOSON STARS #################

##################################################
M1= 1.07
M2= 1.16
M3= 1.27
M4= 1.43

tm1= 1005.
tm2= 360.
tm3= 335.
tm4= 218.
##################################################

dataset = np.genfromtxt(fname='../simulations/C006/c2p2B.dat')
t1=(dataset[:,0] - tm1)/M1
x1=dataset[:,1]/M1 
y1=dataset[:,2]/M1 
#----------------
tf1, strainf1 = strain(t1, x1, y1, 0.006)
xf1           = strainf1.real

#np.savetxt('h22_C006.out', np.c_[tf1,xf1] )   # x,y,z equal sized 1D arrays

dataset = np.genfromtxt(fname='../simulations/C012/c2p2B.dat')
t2=(dataset[:,0] - tm2)/M2
x2=dataset[:,1]/M2  
y2=dataset[:,2]/M2  
#----------------
myt, mystrain = strain(t2, x2, y2, 0.006)
tf2           = myt
strainf2      = mystrain
xf2           = strainf2.real

#np.savetxt('h22_C012.out', np.c_[tf2,xf2] )   # x,y,z equal sized 1D arrays

dataset = np.genfromtxt(fname='../simulations/C018new/c2p2B.dat')
t3=(dataset[:,0] - tm3)/M3
x3=dataset[:,1]/M3 
y3=dataset[:,2]/M3 
#----------------
tf3, strainf3 = strain(t3, x3, y3, 0.016)
xf3           = strainf3.real

#np.savetxt('h22_C018.out', np.c_[tf3,xf3] )   # x,y,z equal sized 1D arrays

dataset = np.genfromtxt(fname='../simulations/C022new/c2p2B.dat')
t4=(dataset[:,0] - tm4)/M4
x4=dataset[:,1]/M4 
y4=dataset[:,2]/M4 
#----------------
tf4, strainf4 = strain(t4, x4, y4, 0.010)
xf4           = strainf4.real
#----------------

#np.savetxt('h22_C022.out', np.c_[tf4,xf4] )   # x,y,z equal sized 1D arrays

#####################################################
############# DARK BOSON STARS ######################
##################################################
M1= 1.07
M2= 1.16
M3= 1.27
M4= 1.43

tm1_ds= 1005.
tm2_ds= 360.
tm3_ds= 335.
tm4_ds= 218.
################# C006 #################################

dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/c2p2.dat')
tt1_ds=dataset[:,0]
t1_ds=(dataset[:,0] - tm1_ds)/M1
x1_ds=dataset[:,1]/M1 
y1_ds=dataset[:,2]/M1 
##----------------
tf1_ds, strainf1_ds = strain(t1_ds, x1_ds, y1_ds, 0.006)
xf1_ds        = strainf1_ds.real
##----------------
myt1, mystrain1 = strain(tt1_ds, x1_ds, y1_ds, 0.006)
ttf1_ds         = myt1
strain1f1_ds    = mystrain1
xff1_ds         = strain1f1_ds.real

np.savetxt('./strain_DS/h22_C006_ds.out', np.c_[ttf1_ds,xff1_ds] )

################# C012 #################################

dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/c2p2.dat')
tt2_ds=dataset[:,0]
t2_ds=(dataset[:,0] - tm2_ds)/M2
x2_ds=dataset[:,1]/M2  
y2_ds=dataset[:,2]/M2  
#----------------
myt, mystrain = strain(t2_ds, x2_ds, y2_ds, 0.006)
tf2_ds        = myt
strainf2_ds   = mystrain
xf2_ds        = strainf2_ds.real

#----------------
myt2, mystrain2 = strain(tt2_ds, x2_ds, y2_ds, 0.006)
ttf2_ds         = myt2
strain2f2_ds    = mystrain2
xff2_ds         = strain2f2_ds.real

np.savetxt('./strain_DS/h22_C012_ds.out', np.c_[ttf2_ds,xff2_ds] )   # x,y,z equal sized 1D arrays

################# C018 #################################

dataset = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/c2p2.dat')
tt3_ds= dataset[:,0]
t3_ds=(dataset[:,0] - tm3_ds)/M3
x3_ds=dataset[:,1]/M3 
y3_ds=dataset[:,2]/M3 
#----------------
tf3_ds, strainf3_ds = strain(t3_ds, x3_ds, y3_ds, 0.016)
xf3_ds              = strainf3_ds.real
#----------------
ttf3_ds, strain3f3_ds = strain(tt3_ds, x3_ds, y3_ds, 0.016)
xff3_ds               = strain3f3_ds.real

np.savetxt('./strain_DS/h22_C018_ds.out', np.c_[ttf3_ds,xff3_ds] )   # x,y,z equal sized 1D arrays

################# C022 #################################

dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/c2p2.dat')
tt4_ds=dataset[:,0] 
t4_ds=(dataset[:,0] - tm4_ds)/M4
x4_ds=dataset[:,1]/M4 
y4_ds=dataset[:,2]/M4 
#----------------
tf4_ds, strainf4_ds = strain(t4_ds, x4_ds, y4_ds, 0.010)
xf4_ds        = strainf4_ds.real
#----------------
ttf4_ds, strain4f4_ds = strain(tt4_ds, x4_ds, y4_ds, 0.010)
xff4_ds               = strain4f4_ds.real

np.savetxt('./strain_DS/h22_C022_ds.out', np.c_[ttf4_ds,xff4_ds] )   # x,y,z equal sized 1D arrays

# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()



axarr[0].plot(tf1_ds, xf1_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(tf1, xf1, 'r:',linewidth = 2,label=r'$C=0.06$ BS')
axarr[0].plot((ct - tm1 - 2000)/M1,  cx*0.4/M1,  'm--',linewidth = 1,label=r'$EOB$')
axarr[0].legend(loc='center left', labelspacing=0.4,prop={'size':15},ncol=1,bbox_to_anchor=(1.0, 0.5))
axarr[0].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[0].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[0].set_xlim([-105.0,85.0])
axarr[0].set_ylim([-0.4,0.4])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$\Re(h^{2,2})$',fontsize=20)

axarr[1].plot(tf2_ds, xf2_ds, 'b',linewidth = 3,label=r'$C=0.12$ DBS')
axarr[1].plot(tf2, xf2, 'b:',linewidth = 2,label=r'$C=0.12$ BS')
axarr[1].plot((ct - tm2 -3988.0)/M2,  0.90*cx/M2,  'm--',linewidth = 1, label=r'$EOB$')
axarr[1].legend(loc='center left', labelspacing=0.4,prop={'size':15},ncol=1,bbox_to_anchor=(1.0, 0.5))
axarr[1].set_xlim([-105.0,85.0])
axarr[1].set_ylim([-0.4,0.4])
axarr[1].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[1].tick_params(axis='x', which='major',  labelsize=13.5)
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False)   
axarr[1].set_ylabel(r'$\Re(h^{2,2})$',fontsize=20)

axarr[2].plot(tf3_ds + 50, xf3_ds, 'k',linewidth = 3,label=r'$C=0.18$ DBS')
axarr[2].plot(tf3 + 20 , xf3, 'k:',linewidth = 2,label=r'$C=0.18$ BS')
axarr[2].plot((ct - tm3 - 4128.0 + 15)/M3,  -0.54*cx/M3,  'm--',linewidth = 1,label=r'$EOB$')
axarr[2].legend(loc='center left', labelspacing=0.4,prop={'size':15},ncol=1,bbox_to_anchor=(1.0, 0.5))
axarr[2].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[2].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[2].set_xlim([-105.0,85.0])
axarr[2].set_ylim([-0.4,0.4])
for label in axarr[2].get_yticklabels()[1::2]:
    label.set_visible(False)   
axarr[2].set_ylabel(r'$\Re(h^{2,2})$',fontsize=20)

axarr[3].plot(tf4_ds , xf4_ds, 'g',linewidth = 3,label=r'$C=0.22$ DBS')
axarr[3].plot(tf4, xf4, 'g:',linewidth = 2,label=r'$C=0.22$ BS')
axarr[3].plot((ct - tm4 - 3830.0)/M4,  -0.85*cx/M4,  'm--',linewidth = 1,label=r'$EOB$')
#axarr[3].plot((ct - tm4 - 4541.0),  0.55*cx,  'm.-',linewidth = 3,label=r'$EOB_{rd}$')
axarr[3].legend(loc='center left', labelspacing=0.4,prop={'size':15},ncol=1,bbox_to_anchor=(1.0, 0.5))
axarr[3].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[3].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[3].set_xlim([-104.0,85.0])
axarr[3].set_ylim([-0.4,0.4])
for label in axarr[3].get_yticklabels()[1::2]:
    label.set_visible(False)   
axarr[3].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=18)
axarr[3].set_ylabel(r'$\Re(h^{2,2})$',fontsize=20)

#axarr[4].plot(tf4_ds, xf4_ds, 'g',linewidth = 3,label=r'$C=0.22$ DBS')
#axarr[4].plot(tf4, xf4, 'g:',linewidth = 2,label=r'$C=0.22$ SB')
#axarr[4].plot((ct - tm4 - 3830.0)/M4,  -0.85*cx/M4,  'm--',linewidth = 3,label=r'$EOB_{ins}$')
#axarr[4].plot((ct - tm4 - 4541.0 - 24),  0.55*cx,  'y--',linewidth = 1,label=r'$EOB_{m}$')
#axarr[4].legend(loc='center left', labelspacing=0.4,prop={'size':15},ncol=1,bbox_to_anchor=(1.0, 0.5))
#axarr[4].tick_params(axis='y', which='major',  labelsize=13.5)
#axarr[4].tick_params(axis='x', which='major',  labelsize=13.5)
#axarr[4].set_xlim([-104.0,85.0])
#axarr[4].set_ylim([-0.4,0.4])
#for label in axarr[4].get_yticklabels()[1::2]:
#    label.set_visible(False)   
#axarr[4].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=20)
#axarr[4].set_ylabel(r'$\Re(h^{2,2})$',fontsize=20)


pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=.5, hspace=0.4)
#pl.tight_layout()

pl.draw()

pl.savefig("gwstrain5_ds.pdf",  bbox_inches="tight")
#pl.show()

