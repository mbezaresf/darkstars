#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

#had2ms = 0.004926420937225289
figure()
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

## DATA SOLITONIC BOSON STARS
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Rext=50
tm1= 950.
tm2= 300.
tm3= 335.
tm4= 218.

Mi1= 1.07
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43


dataset = np.genfromtxt(fname='../simulations/C006/lum_gw.dat')
t11=(dataset[:,3] - Rext - tm1)/Mi1
x11=dataset[:,1]
y11=dataset[:,2]
tt11=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C012/lum_gw.dat')
t12=(dataset[:,3] - Rext - tm2)/Mi2
x12=dataset[:,1]
y12=dataset[:,2]
tt12=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C018/lum_gw.dat')
t13=(dataset[:,3] - Rext - tm3)/Mi3
x13=dataset[:,1]
y13=dataset[:,2]
tt13=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C022/lum_gw.dat')
t14=(dataset[:,3] - Rext - tm4)/Mi4
x14=dataset[:,1]
y14=dataset[:,2]
tt14=dataset[:,0]

## DATA DARK BOSON STARS
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/lum_gw.dat')
t11_ds  = (dataset[:,2] - Rext - tm1)/Mi1
x11_ds  = dataset[:,1]
y11_ds  = dataset[:,2]
tt11_ds = dataset[:,0]


dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/lum_gw.dat')
t12_ds  = (dataset[:,2] - Rext - tm2)/Mi2
x12_ds  = dataset[:,1]
y12_ds  = dataset[:,2]
tt12_ds = dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/lum_gw.dat')
t13_ds  = (dataset[:,2] - Rext - tm3)/Mi3
x13_ds  = dataset[:,1]
y13_ds  = dataset[:,2]
tt13_ds = dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/lum_gw.dat')
t14_ds  = (dataset[:,2] - Rext - tm4)/Mi4
x14_ds  = dataset[:,1]
y14_ds  = dataset[:,2]
tt14_ds = dataset[:,0]


figure(1)
pl.semilogy()
pl.plot(t11_ds, x11_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
pl.plot(t11, x11, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
pl.plot(t12_ds, x12_ds, 'b',linewidth = 2,label=r'$C=0.12$ DBS')
pl.plot(t12, x12, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
pl.plot(t13_ds, x13_ds, 'k',linewidth = 2,label=r'$C=0.18$ DBS')
pl.plot(t13, x13, 'k-.',linewidth = 3,label=r'$C=0.18$ BS')
pl.plot(t14_ds, x14_ds, 'g',linewidth = 2,label=r'$C=0.22$ DBS')
pl.plot(t14, x14, 'g:',linewidth = 3,label=r'$C=0.22$ BS')
pl.legend(loc='lower right', labelspacing=0.5,prop={'size':16},ncol=2)
pl.tick_params(axis='y', which='major',  labelsize=12)
pl.tick_params(axis='x', which='major',  labelsize=12)
pl.xlim([-200.0,1000])
pl.ylim([1e-9,2e-3])
pl.ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)
pl.xlabel(r'$(t - t_{c})/M_0$',fontsize=18)

pl.tight_layout(pad=0.1)
#pl.subplots_adjust(wspace=.5, hspace=0.1)
#pl.tight_layout()

pl.draw()

#pl.savefig("log_lum_gw.pdf")
#pl.show()


figure(2)
pl.plot(t11_ds, x11_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
pl.plot(t11, x11, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
pl.plot(t12_ds, x12_ds, 'b',linewidth = 2,label=r'$C=0.12$ DBS')
pl.plot(t12, x12, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
pl.plot(t13_ds, x13_ds, 'k',linewidth = 2,label=r'$C=0.18$ DBS')
pl.plot(t13, x13, 'k-.',linewidth = 3,label=r'$C=0.18$ BS')
pl.plot(t14_ds, x14_ds, 'g',linewidth = 2,label=r'$C=0.22$ DBS')
pl.plot(t14, x14, 'g:',linewidth = 3,label=r'$C=0.22$ BS')
pl.legend(loc='upper right', labelspacing=0.5,prop={'size':16},ncol=2)
pl.tick_params(axis='y', which='major',  labelsize=12)
pl.tick_params(axis='x', which='major',  labelsize=12)
pl.xlim([-200.0,1000])
pl.ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)
pl.xlabel(r'$(t - t_{c})/M_0$',fontsize=18)

pl.tight_layout(pad=0.1)
#pl.subplots_adjust(wspace=.5, hspace=0.1)
#pl.tight_layout()

pl.draw()

#pl.savefig("lum_gw.pdf")
#pl.show()

figure(3)
# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()


axarr[0].plot(t11_ds, x11_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(t11, x11, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
axarr[0].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[0].legend(handles=[l1],loc='4', labelspacing=0.2,prop={'size':12},ncol=2)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=15)
axarr[0].tick_params(axis='x', which='major',  labelsize=15)
axarr[0].set_xlim([-198.0,1000.0])
#axarr[0].set_ylim([-0.009,0.013])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)


axarr[1].plot(t12_ds, x12_ds, 'b',linewidth = 2,label=r'$C=0.12$ DBS')
axarr[1].plot(t12, x12, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
axarr[1].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[1].tick_params(axis='y', which='major',  labelsize=15)
axarr[1].tick_params(axis='x', which='major',  labelsize=15)
axarr[1].set_xlim([-198.0,580.0])
#axarr[1].set_ylim([0.0,0.0004])    
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[1].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)

axarr[2].plot(t13_ds + 90, x13_ds, 'k',linewidth = 2,label=r'$C=0.18$ DBS')
axarr[2].plot(t13 + 90, x13, 'k-.',linewidth = 3,label=r'$C=0.18$ BS')
axarr[2].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[2].tick_params(axis='y', which='major',  labelsize=15)
axarr[2].tick_params(axis='x', which='major',  labelsize=15)
axarr[2].set_xlim([-198.0,180.0])
axarr[2].set_ylim([0.0,0.0011])    
for label in axarr[2].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[2].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)


axarr[3].plot(t14_ds, x14_ds, 'g',linewidth = 2,label=r'$C=0.22$ DBS')
axarr[3].plot(t14, x14, 'g:',linewidth = 3,label=r'$C=0.22$ BS')
axarr[3].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[3].set_xlabel('t',fontsize=20)
axarr[3].tick_params(axis='y', which='major',  labelsize=15)
axarr[3].tick_params(axis='x', which='major',  labelsize=15)
axarr[3].set_xlim([-145.0,150.0])
#axarr[3].set_ylim([-0.045,0.057])    
for label in axarr[3].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[3].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)
axarr[3].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=20)



pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.3)

#pl.tight_layout()

pl.draw()

pl.savefig("lum_gw2.pdf",bbox_inches="tight")
#pl.show()


figure(4)
# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')

axarr[0].semilogy()
axarr[0].plot(t11_ds, x11_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(t11, x11, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
axarr[0].legend(loc='lower right', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[0].legend(handles=[l1],loc='4', labelspacing=0.2,prop={'size':12},ncol=2)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=12)
axarr[0].tick_params(axis='x', which='major',  labelsize=12)
#axarr[0].set_xlim([-195.0,2000.0])
#axarr[0].set_ylim([1e-9,1e-9])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)

axarr[1].semilogy()
axarr[1].plot(t12_ds, x12_ds, 'b',linewidth = 2,label=r'$C=0.12$ DBS')
axarr[1].plot(t12, x12, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
axarr[1].legend(loc='lower right', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
#axarr[1].set_xlim([-198.0,580.0])
#axarr[1].set_ylim([-0.021,0.03])    
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[1].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)

axarr[2].semilogy()
axarr[2].plot(t13_ds, x13_ds, 'k',linewidth = 2,label=r'$C=0.18$ DBS')
axarr[2].plot(t13, x13, 'k-.',linewidth = 3,label=r'$C=0.18$ BS')
axarr[2].legend(loc='lower right', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[2].tick_params(axis='y', which='major',  labelsize=12)
axarr[2].tick_params(axis='x', which='major',  labelsize=12)
#axarr[2].set_xlim([-198.0,100.0])
#axarr[2].set_ylim([-0.044,0.057])    
for label in axarr[2].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[2].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)

axarr[3].semilogy()
axarr[3].plot(t14_ds, x14_ds, 'g',linewidth = 2,label=r'$C=0.22$ DBS')
axarr[3].plot(t14, x14, 'g:',linewidth = 3,label=r'$C=0.22$ BS')
axarr[3].legend(loc='lower right', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[3].set_xlabel('t',fontsize=20)
axarr[3].tick_params(axis='y', which='major',  labelsize=12)
axarr[3].tick_params(axis='x', which='major',  labelsize=12)
#axarr[3].set_xlim([-195.0,150.0])
#axarr[3].set_ylim([-0.045,0.057])    
for label in axarr[3].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[3].set_ylabel(r'$ \left(dE/dt \right)_{GW}$',fontsize=18)
axarr[3].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=18)



pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.3)

#pl.tight_layout()

pl.draw()

#pl.savefig("log_lum_gw2.pdf")
#pl.show()




