#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *




def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')


# c1 is the compactness with M/R_M, but from the simulations
# we can only calculate c2=M/R_N, so we will use that one
# freq is the frequency of the gravitational QNM obtained
# from the psi4 by perturbing a single star of mass M=1
# freq2 is the next peak of the gravitational QNM 

#c1=[0.06,0.12,0.18,0.22,0.28]
c2=[0.067,0.128,0.197,0.228,0.284]
freq=[0.0138,0.0232,0.0315,0.0355,0.0479]
omega = 2*np.pi*np.array(freq)

# cb is the compactness M/R_N of the remnant from the simulations
# omegab is Mf*omega as stated in the last column of table II
#cb=[0.101,0.213,0.42]
#omegab=[0.117,0.203,0.329]

cb=[0.101,0.213,0.42,0.50,0.565]
omegab=[0.117,0.203,0.329,0.373,0.5]


#cc=[0.067,0.128,0.197,0.228]
#omegac=[0.017,0.045,0.087,0.108]
#omegac = 2*np.array(omegac)


#a3,a2,a1,a0 = np.polyfit(c2, omega, 3)
#print a2
#x   = np.arange(0.06, 0.3, 0.01)
#ybs = a3*x*x*x + a2*x*x + a1*x + a0
#pl.plot(x, ybs, 'c',linestyle='--',linewidth=2)



#omega3=[m*0.06+b,m*0.12+b,m*0.18+b,m*0.22+b,m*0.28+b]
#freq4=[m2*0.06+b2,m2*0.12+b2,m2*0.18+b2,m2*0.22+b2,m2*0.28+b2]

#freq006 = [0.0141176]

# Two subplots not sharing x axes
#f, axarr = pl.subplots(1, sharex=False)

#pl.figure(figsize=(8,8))

pl.ylim(0.002, 0.39)
pl.xlim(0.0, 0.45)

##ax.set_yscale('log')
#pl.semilogy()

figure(1)


#pl.plot(c2, omega, 'c--')
pl.plot(c2[0], omega[0], 'r',marker='o',markersize=10,linestyle='None',label=r'$C=0.06$' )
pl.plot(c2[1], omega[1], 'b',marker='o',markersize=10,linestyle='None',label=r'$C=0.12$' )
pl.plot(c2[2], omega[2], 'k',marker='o',markersize=10,linestyle='None',label=r'$C=0.18$' )
pl.plot(c2[3], omega[3], 'g',marker='o',markersize=10,linestyle='None',label=r'$C=0.22$' )
#pl.plot(c2[4], omega[4], 'm',linestyle=':',marker='o',markersize=8, label=r'$C=0.28$' )


pl.plot(cb[0], omegab[0], 'r',marker='s',markersize=10)
pl.plot(cb[1], omegab[1], 'b',marker='s',markersize=10)
pl.plot(cb[2], omegab[2], 'k',marker='s',markersize=10,markeredgewidth=2,markeredgecolor='k',
markerfacecolor='None') 
#pl.plot(cb[3], omegab[3], 'g',marker='^',markersize=10)
#pl.plot(cb[4], omegab[4], 'g',marker='s',markersize=10)
#pl.plot(c1[3], omega[3], 'g',linestyle=':',marker='^',markersize=8, label=r'$C=0.22m$' )
#pl.plot(c1[4], omega[4], 'm',linestyle=':',marker='^',markersize=8, label=r'$C=0.28m$' )


#pl.plot(c1, freq2, 'b',linestyle='_',marker='o',markersize=10)
#pl.plot(cb, omegab, 'c',linestyle=':',marker='s',markersize=10)

#pl.plot(cc, omegac, 'r',marker='^',markersize=10)

#x   = np.arange(0.06, 0.3, 0.01)
#ybs = m*x + b
#yns =-0.0408803 + 2.25395*x

#pl.plot(x, ybs, 'r',linestyle='--',linewidth=2)
#pl.plot(x, yns, 'r',linestyle=':',linewidth=2)


#pl.plot(fc2, fp2, 'k',label='BS')
#pl.plot(fc1, fp1, 'r',label='NS')

pl.legend(loc='upper left', labelspacing=0.2,prop={'size':19},ncol=1)
#pl.legend(loc='lower right', labelspacing=0.2,prop={'size':20},ncol=1)
#pl.legend(bbox_to_anchor=(1.28, 1),loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$C_N$',fontsize=20)
pl.ylabel(r'$M \omega$',fontsize=20)
#pl.xlim(0.004, 0.15)
#pl.ylim(2E-6, 5E-1)
#pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
pl.xticks(fontsize=16)
pl.yticks(fontsize=16)
#plt.tick_params(axis='x', labelbottom='off')
#plt.tick_params(axis='y', labelleft='off')
savefig('freqvscomp3.pdf')





