#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

#had2ms = 0.004926420937225289
figure()
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')


################### DARK STARS ################################

################################################################
################## DATA MASS ###################################
################################################################

Rext=50
tm1_ds = 950.
tm2_ds  = 300.
tm3_ds  = 330.
tm4_ds  = 218.

Mi1= 1.07
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43


dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/mass.dat')
t1_ds=(dataset[:,0] - Rext - tm1_ds)/Mi1
m1_ds=dataset[:,1] 

dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/mass.dat')
t2_ds=(dataset[:,0] - Rext - tm2_ds)/Mi2
m2_ds=dataset[:,1] 


dataset = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/mass.dat')
t3_ds=(dataset[:,0] - Rext - tm3_ds)/Mi3
m3_ds=dataset[:,1] 


dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/mass.dat')
t4_ds=(dataset[:,0] - Rext - tm4_ds)/Mi4
m4_ds=dataset[:,1] 

################################################################
################## DATA J    ###################################
################################################################


dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/Jz.dat')
tt1_ds=(dataset[:,0] - Rext - tm1_ds)/Mi1
j1_ds=dataset[:,1]/1.16
print dataset[1,1] 

r1_ds = 10
j1_av_ds = movingaverage(j1_ds, r1_ds)


dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/Jz.dat')
tt2_ds=(dataset[:,0] - Rext - tm2_ds)/Mi2
j2_ds=dataset[:,1]/1.22 

r2_ds = 10
j2_av_ds = movingaverage(j2_ds, r2_ds)

dataset = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/Jz.dat')
tt3_ds=(dataset[:,0] - Rext - tm3_ds)/Mi3
j3_ds=dataset[:,1] 

r3_ds = 1
j3_av_ds = movingaverage(j3_ds, r3_ds)

dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/Jz.dat')
tt4_ds=(dataset[:,0] - Rext - tm4_ds)/Mi4
j4_ds=dataset[:,1] 

r4_ds = 1
j4_av_ds = movingaverage(j4_ds, r4_ds)

################### SOLITONIC BOSON STARS ######################

################################################################
################## DATA MASS ###################################
################################################################

Rext=50
tm1= 950.
tm2= 300.
tm3= 330.
tm4= 218.

Mi1= 1.07
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43


dataset = np.genfromtxt(fname='../simulations/C006/massB.dat')
t1=(dataset[:,0] - Rext - tm1)/Mi1
m1=dataset[:,1] 

dataset = np.genfromtxt(fname='../simulations/C012/massB.dat')
t2=(dataset[:,0] - Rext - tm2)/Mi2
m2=dataset[:,1] 


dataset = np.genfromtxt(fname='../simulations/C018new/massB.dat')
t3=(dataset[:,0] - Rext - tm3)/Mi3
m3=dataset[:,1] 


dataset = np.genfromtxt(fname='../simulations/C022new/massB.dat')
t4=(dataset[:,0] - Rext - tm4)/Mi4
m4=dataset[:,1] 

################################################################
################## DATA J    ###################################
################################################################


dataset = np.genfromtxt(fname='../simulations/C006/JzB.dat')
tt1=(dataset[:,0] - Rext - tm1)/Mi1
j1=dataset[:,1]/1.34  

r1 = 150
j1_av = movingaverage(j1, r1)


dataset = np.genfromtxt(fname='../simulations/C012/JzB.dat')
tt2=(dataset[:,0] - Rext - tm2)/Mi2
j2=dataset[:,1]/1.22
print j2[0]
r2 = 50
j2_av = movingaverage(j2, r2)

dataset = np.genfromtxt(fname='../simulations/C018new/JzB.dat')
tt3=(dataset[:,0] - Rext - tm3)/Mi3
j3=dataset[:,1] 


r3 = 10
j3_av = movingaverage(j3, r3)

dataset = np.genfromtxt(fname='../simulations/C022new/JzB.dat')
tt4=(dataset[:,0] - Rext - tm4)/Mi4
j4=dataset[:,1] 

r4 = 1
j4_av = movingaverage(j4, r4)


pl.plot(tt1_ds, abs(j1_av_ds), 'r',linewidth = 3,label=r'$C=0.06$ DBS')
pl.plot(tt2_ds[0:tt2_ds.shape[0]-10], abs(j2_av_ds[0:j2_av_ds.shape[0]-10]), 'b',linewidth = 3,label=r'$C=0.12$ DBS')
pl.plot(tt1[0:tt1.shape[0]-50], abs(j1_av[0:j1_av.shape[0]-50]), 'r--',linewidth = 1,label=r'$C=0.06$ BS')
pl.plot(tt2[0:tt2.shape[0]-10], abs(j2_av[0:j2_av.shape[0]-10]), 'b--',linewidth = 1,label=r'$C=0.12$ BS')
legend(loc='lower right', labelspacing=0.8,prop={'size':20},ncol=1)
xlim([-120.0,1500.0])
ylim([0.001,1.1])    
tick_params(axis='y', which='major',  labelsize=15)
tick_params(axis='x', which='major',  labelsize=15)
ylabel(r'$J_{z}/J_{z}(t=0)$',fontsize=20)
xlabel(r'$(t - t_{c})/M_0$',fontsize=20)

#pl.show()
#pl.tight_layout(pad=0.1)
#plt.subplots_adjust(wspace=.5, hspace=0.1)
#pl.tight_layout()

pl.draw()

pl.savefig("J_DS.pdf",bbox_inches="tight")


exit()
# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()


axarr[0].plot(t1_ds, m1_ds, 'r',linewidth = 1,linestyle='-',label=r'$C=0.06$ DBS')
axarr[0].plot(t1, m1, 'r',linewidth = 3,linestyle=':',label=r'$C=0.06$ BS')
axarr[0].plot(t2_ds, m2_ds, 'b',linewidth = 1,linestyle='-',label=r'$C=0.12$ DBS')
axarr[0].plot(t2, m2, 'b',linewidth = 3,linestyle=':',label=r'$C=0.12$ BS')
#axarr[0].plot(t3, m3, 'k',linewidth = 3,linestyle=':',label=r'$C=0.18$ SB')
#axarr[0].plot(t3_ds, m3_ds, 'k',linewidth = 1,linestyle='-',label=r'$C=0.18$ DSB')
#axarr[0].plot(t4, m4, 'g',linewidth = 3,linestyle=':',label=r'$C=0.22$ SB')
#axarr[0].plot(t4_ds, m4_ds, 'g',linewidth = 1,linestyle='-',label=r'$C=0.22$ DSB')
axarr[0].legend(loc='lower right', labelspacing=0.8,bbox_to_anchor=(0.22, 1.05),prop={'size':9.5},ncol=4)
axarr[0].tick_params(axis='y', which='major',  labelsize=12)
axarr[0].tick_params(axis='x', which='major',  labelsize=12)
axarr[0].set_xlim([-120.0,1500.0])
#axarr[0].set_xlim([10.0,1816.0])
#axarr[0].set_ylim([0.71,1.49])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
axarr[0].set_ylabel(r'$M_{ADM}$',fontsize=18)



axarr[1].plot(tt1_ds, abs(j1_av_ds), 'r-',linewidth = 1,label=r'$C=0.06$ DBS')
axarr[1].plot(tt1[0:tt1.shape[0]-50], abs(j1_av[0:j1_av.shape[0]-50]), 'r:',linewidth = 3,label=r'$C=0.06$ BS')
axarr[1].plot(tt2_ds[0:tt2_ds.shape[0]-10], abs(j2_av_ds[0:j2_av_ds.shape[0]-10]), 'b-',linewidth = 1,label=r'$C=0.12$ DBS')
axarr[1].plot(tt2[0:tt2.shape[0]-10], abs(j2_av[0:j2_av.shape[0]-10]), 'b:',linewidth = 3,label=r'$C=0.12$ BS')
#axarr[1].plot(tt3, abs(j3_av), 'k:',linewidth = 3,label=r'$C=0.18$ SB')
#axarr[1].plot(tt3_ds, abs(j3_av_ds), 'k-',linewidth = 1,label=r'$C=0.18$ DSB')
#axarr[1].plot(tt4, abs(j4_av), 'g:',linewidth = 3,label=r'$C=0.22$ SB')
#axarr[1].plot(tt4_ds, abs(j4_av_ds), 'g-',linewidth = 1,label=r'$C=0.22$ DSB')
axarr[1].legend(loc='upper right', labelspacing=0.5,prop={'size':15},ncol=2)
axarr[1].set_xlim([-120.0,1500.0])
axarr[1].set_ylim([0.001,1.6])    
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
#axarr[1].set_ylim([-0.6,0.1])
#for label in axarr[1].get_yticklabels()[1::2]:
#    label.set_visible(False)   
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
axarr[1].set_ylabel(r'$J_{z}$',fontsize=18)
axarr[1].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=18)




pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=.5, hspace=0.1)
#pl.tight_layout()

pl.draw()

pl.savefig("adm_J_DS.pdf")
#pl.show()



