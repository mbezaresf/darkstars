#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

#had2ms = 0.004926420937225289

mass1 =1.0
#mass1 = 0.36501632425647906364
#r,a,alpha,phi0,PHI,psi,p,rho,epp
dataset = np.genfromtxt(fname='./id/C006/phir1d.dat')
x1 = dataset[:,0]
x2 = dataset[:,1] 

dataset = np.genfromtxt(fname='./id/C012/phir1d.dat')
y1 = dataset[:,0]
y2 = dataset[:,1] 

dataset = np.genfromtxt(fname='./id/C018/phir1d.dat')
z1 = dataset[:,0]
z2 = dataset[:,1] 

dataset = np.genfromtxt(fname='./id/C022/phir1d.dat')
w1 = dataset[:,0]
w2 = dataset[:,1] 



figure(4)
pl.plot(x1, x2, 'r',linewidth = 2,label=r'$C=0.06$')
pl.plot(y1, y2, 'b',linewidth = 2,label=r'$C=0.12$')
pl.plot(z1, z2, 'k',linewidth = 2,label=r'$C=0.18$')
pl.plot(w1, w2, 'g',linewidth = 2,label=r'$C=0.22$')
pl.legend(loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$r$',fontsize=20)
pl.tick_params(axis='both', which='major', labelsize=15)
pl.ylabel(r'$\phi(r)$',fontsize=20)
pl.xlim(0.0, 15.0)
pl.ylim(0.0, 0.04)
pl.savefig('phi.pdf',bbox_inches="tight")

figure(5)
pl.plot(x1, x2, 'r',linewidth = 2,label=r'$C=0.06$')
pl.plot(y1, y2, 'b',linewidth = 2,label=r'$C=0.12$')
pl.plot(z1, z2, 'k',linewidth = 2,label=r'$C=0.18$')
pl.plot(w1, w2, 'g',linewidth = 2,label=r'$C=0.22$')
pl.legend(loc='upper right', labelspacing=0.2,prop={'size':20},ncol=1)
pl.xlabel(r'$r$',fontsize=15)
pl.ylabel(r'$\phi(r)$',fontsize=15)
pl.xlim(0.0, 15.0)
pl.ylim(0.0, 0.04)
#savefig('phi_r.pdf')

