#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

#had2ms = 0.004926420937225289
figure()
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

################################################################
################## DATA GW SOLITONIC BOSON STARS ###############
################################################################

Rext=50
tm1= 950.
tm2= 300.
tm3= 335.
tm4= 218.

Mi1= 1.07
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43


dataset = np.genfromtxt(fname='../simulations/C006/c2p2B.dat')
tt1 = dataset[:,0] 
t1=(dataset[:,0] - Rext - tm1)/Mi1
x1=dataset[:,1]
y1=dataset[:,2]
norm1=sqrt(x1*x1+y1*y1) 

#t1f = t1-30.0
r1 = 10
x1_av = movingaverage(x1, r1)

dataset = np.genfromtxt(fname='../simulations/C012/c2p2B.dat')
tt2 = dataset[:,0]
t2=(dataset[:,0] - Rext - tm2)/Mi2
x2=dataset[:,1] 
y2=dataset[:,2]
norm2=sqrt(x2*x2+y2*y2)
#t1f = t1-30.0
r2 = 10
x2_av = movingaverage(x2, r2)

dataset = np.genfromtxt(fname='../simulations/C018new/c2p2B.dat')
tt3 = dataset[:,0]
t3=(dataset[:,0] - Rext - tm3)/Mi3
x3=dataset[:,1] 
y3=dataset[:,2]
norm3=sqrt(x3*x3+y3*y3)
#t1f = t1-30.0
r3 = 10
x3_av = movingaverage(x3, r3)

dataset = np.genfromtxt(fname='../simulations/C022new/c2p2B.dat')
tt4 = dataset[:,0]
t4=(dataset[:,0] - Rext - tm4)/Mi4
x4=dataset[:,1]
y4=dataset[:,2]
norm4=sqrt(x4*x4+y4*y4) 

#t1f = t1-30.0
r4 = 10
x4_av = movingaverage(x4, r4)

#print "Max value element : ", max(norm1), max(norm2),max(norm3),max(norm4)
#np.savetxt('norm1', np.c_[tt1,norm1] ) 
#np.savetxt('norm2', np.c_[tt2,norm2] ) 
#np.savetxt('norm3', np.c_[tt3,norm3] ) 
#np.savetxt('norm4', np.c_[tt4,norm4] ) 
#exit()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataset = np.genfromtxt(fname='../simulations/C006/lum_gw.dat')
t11=(dataset[:,3] - Rext - tm1)/Mi1
x11=dataset[:,1]
y11=dataset[:,2]
tt11=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C012/lum_gw.dat')
t12=(dataset[:,3] - Rext - tm2)/Mi2
x12=dataset[:,1]
y12=dataset[:,2]
tt12=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C018/lum_gw.dat')
t13=(dataset[:,3] - Rext - tm3)/Mi3
x13=dataset[:,1]
y13=dataset[:,2]
tt13=dataset[:,0]

dataset = np.genfromtxt(fname='../simulations/C022/lum_gw.dat')
t14=(dataset[:,3] - Rext - tm4)/Mi4
x14=dataset[:,1]
y14=dataset[:,2]
tt14=dataset[:,0]


################################################################
################## DATA GW  DARK STARS            ##############
################################################################

Rext=50
tm1_ds= 950.
tm2_ds= 300.
tm3_ds= 335.
tm4_ds= 218.

Mi1= 1.08
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43

dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/c2p2.dat')
tt1_ds = dataset[:,0] 
t1_ds = (dataset[:,0] - Rext - tm1_ds)/Mi1
x1_ds = dataset[:,1]
y1_ds = dataset[:,2]
norm1_ds = sqrt(x1_ds*x1_ds+y1_ds*y1_ds) 

#t1f_ds = t1_ds-30.0
r1_ds = 10
x1_av_ds = movingaverage(x1_ds, r1_ds)

dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/c2p2.dat')
tt2_ds = dataset[:,0] 
t2_ds=(dataset[:,0] - Rext - tm2_ds)/Mi2
x2_ds=dataset[:,1] 
y2_ds=dataset[:,2]
norm2_ds=sqrt(x2_ds*x2_ds+y2_ds*y2_ds)
r2_ds = 10
x2_av_ds = movingaverage(x2_ds, r2_ds)

dataset = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/c2p2.dat')
tt3_ds = dataset[:,0]
t3_ds=(dataset[:,0] - Rext - tm3_ds)/Mi3
x3_ds=dataset[:,1] 
y3_ds=dataset[:,2]
norm3_ds=sqrt(x3_ds*x3_ds+y3_ds*y3_ds)
r3_ds = 10
x3_av_ds = movingaverage(x3_ds, r3_ds)

dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/c2p2.dat')
tt4_ds = dataset[:,0]
t4_ds=(dataset[:,0] - Rext - tm4_ds)/Mi4
x4_ds=dataset[:,1]
y4_ds=dataset[:,2]
norm4_ds=sqrt(x4_ds*x4_ds+y4_ds*y4_ds) 
r4_ds = 10
x4_av_ds = movingaverage(x4_ds, r4_ds)

#print "Max value element : ", max(norm1_ds),max(norm2_ds),max(norm3_ds),max(norm4_ds)
#np.savetxt('./FT_DS/DS/norm1_ds', np.c_[tt1_ds,norm1_ds] ) 
#np.savetxt('./FT_DS/DS/norm2_ds', np.c_[tt2_ds,norm2_ds] ) 
#np.savetxt('./FT_DS/DS/norm3_ds', np.c_[tt3_ds,norm3_ds] ) 
#np.savetxt('./FT_DS/DS/norm4_ds', np.c_[tt4_ds,norm4_ds] ) 
#exit()



# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()


axarr[0].plot(t1_ds, x1_ds, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(t1, x1, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
axarr[0].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[0].legend(handles=[l1],loc='4', labelspacing=0.2,prop={'size':12},ncol=2)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=15)
axarr[0].tick_params(axis='x', which='major',  labelsize=15)
axarr[0].set_xlim([-280.0,800.0])
axarr[0].set_ylim([-0.009,0.013])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$\Re(r\, \Psi^{2,2}_{4})$',fontsize=20)


axarr[1].plot(t2_ds, x2_ds, 'b',linewidth = 3,label=r'$C=0.12$ DBS')
axarr[1].plot(t2, x2, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
axarr[1].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[1].set_xlabel('t',fontsize=20)
axarr[1].tick_params(axis='y', which='major',  labelsize=15)
axarr[1].tick_params(axis='x', which='major',  labelsize=15)
axarr[1].set_xlim([-199.0,510.0])
axarr[1].set_ylim([-0.021,0.03])    
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[1].set_ylabel(r'$\Re(r\, \Psi^{2,2}_{4})$',fontsize=20)

axarr[2].plot(t3_ds + 90, x3_ds, 'k',linewidth = 3,label=r'$C=0.18$ DBS')
axarr[2].plot(t3 + 90, x3, 'k--',linewidth = 1,label=r'$C=0.18$ BS')
axarr[2].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[2].set_xlabel('t',fontsize=20)
axarr[2].tick_params(axis='y', which='major',  labelsize=15)
axarr[2].tick_params(axis='x', which='major',  labelsize=15)
axarr[2].set_xlim([-150.0,140.0])
axarr[2].set_ylim([-0.044,0.057])    
for label in axarr[2].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[2].set_ylabel(r'$\Re(r\, \Psi^{2,2}_{4})$',fontsize=20)


axarr[3].plot(t4_ds, x4_ds, 'g',linewidth = 3,label=r'$C=0.22$ DBS')
axarr[3].plot(t4, x4, 'g--',linewidth = 1,label=r'$C=0.22$ BS')
axarr[3].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[3].tick_params(axis='y', which='major',  labelsize=15)
axarr[3].tick_params(axis='x', which='major',  labelsize=15)
axarr[3].set_xlim([-100.0,100.0])
axarr[3].set_ylim([-0.045,0.057])    
for label in axarr[3].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[3].set_ylabel(r'$\Re(r\, \Psi^{2,2}_{4})$',fontsize=20)
axarr[3].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=20)



pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.3)

#pl.tight_layout()

pl.draw()

pl.savefig("gw_ampl2_ds.pdf",bbox_inches="tight")
#pl.show()

