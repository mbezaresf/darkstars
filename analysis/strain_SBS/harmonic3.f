
      program      main
      implicit     none

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     decomposition into spin-weighted spherical harmonics
!
!     cos(theta) = z/r,  tg(phi) = y/x
!
!     u = u(phi, theta)
!
!     phi = {0, 2*pi}, theta = {0, pi}
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      
            
      integer :: i, j, k, N, itheta, iphi, output, initial, itime
      integer :: correction, integration
      real*8  :: pi, theta, phi, integral, h, h2, x, y, integralc(2)
      real*8  :: rp, ip, norm, dt, factor, temp1, time_tort
      real*8  :: massI, rext, test, smallnumber, ra, rr, fpR, fpI
      real*8  :: surface_integral,surface_integral2
            
! ------- this parameter changes depending on the size of u------------
      integer Ndim
!      parameter ( Ndim = 80 )


      character*20    file_psi
      integer         read_shape(2), shape(2)
      character*5     read_cnames
      integer         read_rank
      real*8          read_coords(1000), bbox(4) 
      real*8          read_time, time, timeold, time_min
      
      integer         iargc, gf3_rc, gft_read_full,index
      character*5     name, sarg
      external        sarg, gft_read_full
      character*32    realpsiname,imgpsiname,massname,jzname

      real*8  :: tY2m2,tY2m1,tY20,tY2p1
      real*8  :: tY2p2,tY3m3,tY3m2,tY3m1
      real*8  :: tY30,tY3p1,tY3p2,tY3p3
      real*8  :: tY4m4,tY4m3,tY4m2,tY4m1
      real*8  :: tY40,tY4p1,tY4p2,tY4p3
      real*8  :: cI2p2,cR2p2,dtcR2p2,dtCI2p2
      real*8  :: d2p2(2),d2p1(2),d20(2),d2m1(2),d2m2(2)
      real*8  :: d2p2_inf(2),d2p1_inf(2),d20_inf(2),d2m1_inf(2)
      real*8  :: d2m2_inf(2)

      integer     MAXMEM
!      parameter ( MAXMEM = 1000000 )
      parameter ( MAXMEM = 14000 )
      real*8  :: rpsi(MAXMEM),ipsi(MAXMEM),mass(MAXMEM),Jz(MAXMEM)
      real*8  :: Y2m2(2,MAXMEM),Y2m1(2,MAXMEM),Y20(2,MAXMEM),Y2p1(2,MAXMEM)
      real*8  :: Y2p2(2,MAXMEM),Y3m3(2,MAXMEM),Y3m2(2,MAXMEM),Y3m1(2,MAXMEM)
      real*8  :: Y30(2,MAXMEM),Y3p1(2,MAXMEM),Y3p2(2,MAXMEM),Y3p3(2,MAXMEM)
      real*8  :: Y4m4(2,MAXMEM),Y4m3(2,MAXMEM),Y4m2(2,MAXMEM),Y4m1(2,MAXMEM)
      real*8  :: Y40(2,MAXMEM),Y4p1(2,MAXMEM),Y4p2(2,MAXMEM),Y4p3(2,MAXMEM)
      real*8  :: Y4p4(2,MAXMEM), temp(2,MAXMEM)

      integer     MAXTIME
      parameter ( MAXTIME = 10000 )
      real*8  :: c2m2(2,MAXTIME),c2m1(2,MAXTIME),c20(2,MAXTIME)
      real*8  :: c2p2(2,MAXTIME),c2p1(2,MAXTIME)
      real*8  :: c2m2_inf(2,MAXTIME),c2m1_inf(2,MAXTIME),c20_inf(2,MAXTIME)
      real*8  :: c2p2_inf(2,MAXTIME),c2p1_inf(2,MAXTIME)
      real*8  :: c3m3(2,MAXTIME),c3m2(2,MAXTIME),c3m1(2,MAXTIME),c30(2,MAXTIME)
      real*8  :: c3p1(2,MAXTIME),c3p2(2,MAXTIME),c3p3(2,MAXTIME)
      real*8  :: c4m4(2,MAXTIME),c4m3(2,MAXTIME),c4m2(2,MAXTIME),c4m1(2,MAXTIME)
      real*8  :: c40(2,MAXTIME),c4p1(2,MAXTIME),c4p2(2,MAXTIME),c4p3(2,MAXTIME)
      real*8  :: c4p4(2,MAXTIME),tmass(MAXTIME),tJz(MAXTIME),phase(MAXTIME) 
      real*8  :: omega1(MAXTIME),omega2(MAXTIME),LGW(MAXTIME),LGW_inf(MAXTIME)
      real*8  :: d2p2T(2,MAXTIME),h2p2(2,MAXTIME)
      real*8  :: d2p2T_inf(2,MAXTIME),h2p2_inf(2,MAXTIME)

       if (iargc() .lt. 1) then
          write(*,*) '*************************************************'
          write(*,*) '* harmonic2: Produces a decomposition           *'
          write(*,*) '*            into -2 spin-weighted harmonics.   *'
          write(*,*) '* Usage:                                        *'
          write(*,*) '*************************************************'
          write(*,*) '       harmonic2  <surface letter>'
          write(*,*) ''
          write(*,*) '   E.g. harmonic2 b'
          write(*,*) '                *****************'
          stop
       end if
       name = sarg(1,'c')

       realpsiname = 'r'//name(:1)//'p4.sdf'
       imgpsiname  = 'i'//name(:1)//'p4.sdf'
       massname    = name(:1)//'mass.sdf'
       jzname      = name(:1)//'Jz.sdf'

       if (.true.) then
          write(*,*) '   name = ',name
          write(*,*) 'Reading in the following files:'
          write(*,*) '   realpsiname = ',realpsiname
          write(*,*) '   imgpsiname  = ',imgpsiname
          write(*,*) '   massname    = ',massname
          write(*,*) '   jzname      = ',jzname
       end if

       !----reading parameters from a file------------------
       itime = 2                                   
       gf3_rc =  gft_read_full(realpsiname, itime,  read_shape, &
      &     read_cnames, read_rank, read_time, read_coords, rpsi)      
       if (gf3_rc .ne.1) goto 99
       if (read_shape(1)*read_shape(2).gt.MAXMEM) then
          write(*,*)'read_shape(1)/(2) = ',read_shape(1),read_shape(2)
          write(*,*)'read_shape(1)*(2) = ',read_shape(1)*read_shape(2)
          write(*,*)'MAXMEM            = ',MAXMEM
          write(*,*)'Not enough memory, recompile. Quitting.'
          goto 99
       end if

       Ndim = read_shape(2)-1     
       dt   = read_time
  
       print*, "read_time=",read_time
       print*, "read_shape=",read_shape
       print*, "Ndim=",Ndim
       print*,'MAXMEM = ',read_shape(1)*read_shape(2)

!       stop

!-----------------------------------------------
!-----------------------------------------------
       output = 0        !-- 0, only .dat, 1 also output on the screen
       massI  = 1.0      !-- the initial (m1 + m2) of the isolated bodies
       rext   = 50.0     !-- the radius of the surface extraction
       time_min = 84.0    !-- time to start the integration of Ds and Es
       factor  = 1.0     ! arbitrary normalization factor to match other results
      
       N      = Ndim
       pi     = dacos(-1.0d0)
       h      = pi/N
       h2     = h*h

!------------------------------------------------------
      open(unit=110, file="c2m2.dat")
      open(unit=111, file="c2m1.dat")
      open(unit=112, file="c20.dat")
      open(unit=113, file="c2p1.dat")
      open(unit=114, file="c2p2.dat")

      open(unit=120, file="mass.dat")      
      open(unit=121, file="Jz.dat")      
      open(unit=122, file="phase.dat")            

      open(unit=130, file="omega.dat")                        
      open(unit=131, file="c2p2_inf.dat")                        
      open(unit=132, file="lum_gw.dat")                        

      open(unit=133, file="d2p2.dat")
      open(unit=134, file="d2p2_inf.dat")

      open(unit=135, file="h2p2.dat")
      open(unit=136, file="h2p2_inf.dat")

!------------------------------------------------------
      itime = 1                                    

!------------------------------------------------------------            
!-----DEFINE SPIN WEIGHTED SPHERICAL HARMONIC DECOMPOSITION--
!------------------------------------------------------------

!!------computing the spin-weighted spherical harmonics Y'_l,m------------	    
!-----  Y_l,m = Y'_l,m * exp ( +i*m*phi) ---------------------------------
!-----  Y_l,m = Y'_l,m * [cos( +i*m*phi) + i*sin( +i*m*phi)] -------------
!----- *Y_l,m = Y'_l,m * [cos( +i*m*phi) - i*sin( +i*m*phi)] -------------
            
      do iphi = 1, 2*N+1
      phi = (iphi-1)*h
      do itheta = 1, N+1
        theta = (itheta-1)*h   
	x = cos(theta)

        index = (itheta-1)*read_shape(1)+iphi
	    
!        tY2m2 = (x-1)**2*sqrt(5.D0)/sqrt(0.3141592653589793D1)/8
!        tY2m1 = sqrt(x+1)*sqrt(1-x)**3*sqrt(5.D0)/sqrt(0.3141592653589793D1)/4
!        tY20 = -(x-1)*(x+1)*sqrt(5.D0)*sqrt(6.D0)/sqrt(0.3141592653589793D1)/8
!        tY2p1 = sqrt(5.D0)*sqrt(1-x)*sqrt(x+1)**3/sqrt(0.3141592653589793D1)/4
!        tY2p2 = sqrt(5.D0)*(x+1)**2/sqrt(0.3141592653589793D1)/8

        !from Brugman
        tY2m2 = sqrt(5.0/(64.0*pi)) * (1.0 - cos(theta))**2
        tY2m1 =-sqrt(5.0/(16.0*pi)) * sin(theta) * (1.0 - cos(theta))
        tY20  = sqrt(15.0/(32.0*pi)) * (sin(theta))**2
        tY2p1 =-sqrt(5.0/(16.0*pi)) * sin(theta) * (1.0 + cos(theta)) 
        tY2p2 = sqrt(5.0/(64.0*pi)) * (1.0 + cos(theta))**2
       	    		    	    
	Y2m2(1,index) = tY2m2*cos(-2.0*phi)
	Y2m1(1,index) = tY2m1*cos(-1.0*phi)
	Y20(1,index)  = tY20*cos(0.0*phi)
	Y2p1(1,index) = tY2p1*cos(+1.0*phi)
	Y2p2(1,index) = tY2p2*cos(+2.0*phi)

	Y2m2(2,index) =-tY2m2*sin(-2.0*phi)
	Y2m1(2,index) =-tY2m1*sin(-1.0*phi)
	Y20(2,index)  =-tY20*sin(0.0*phi)
	Y2p1(2,index) =-tY2p1*sin(+1.0*phi)
	Y2p2(2,index) =-tY2p2*sin(+2.0*phi)

!       for testing only
!        rpsi(index) = Y2p2(1,index)
!        ipsi(index) =-Y2p2(2,index)
	       	       
      end do
      end do


!-----------------------------------------
      ! for testing only 
      ! first that the integral of 1 is 4*pi
!       mass = 1.0
!       rpsi = 1.0
!       ipsi = 0.0


!       test = surface_integral(mass,MAXMEM,Ndim)                                        
!       print*, "4*pi is",test,"and it should be", 4.0*pi
!       test = surface_integral2(mass,MAXMEM,Ndim)
!       print*, "4*pi is nowd",test,"and it should be", 4.0*pi

!       temp(1,:) = rpsi(:)*Y2p2(1,:) - ipsi(:)*Y2p2(2,:)
!       temp(2,:) = rpsi(:)*Y2p2(2,:) + ipsi(:)*Y2p2(1,:)
!       print*, "N=", N
!       test = surface_integral2(temp(1,:),MAXMEM,Ndim)
!       print*, "Y22 ortonormalization",test
!       test = surface_integral2(temp(2,:),MAXMEM,Ndim)
!       print*, "Y22 ortonormalization",test
!       stop     

!-----------------------------------------
      itime = 0

      do while (.true.)

        itime = itime + 1

        !----reading from a file----------------------             
        gf3_rc =  gft_read_full(realpsiname, itime,  read_shape, &
        &     read_cnames, read_rank, read_time, read_coords, rpsi)      
        if (gf3_rc .ne.1) goto 99
        gf3_rc =  gft_read_full(imgpsiname, itime,  read_shape, &
        &     read_cnames, read_rank, read_time, read_coords, ipsi)            
        if (gf3_rc .ne.1) goto 99
        gf3_rc = gft_read_full(massname,   itime,  read_shape, &
        &     read_cnames, read_rank, read_time, read_coords, mass)            
        if (gf3_rc .ne.1) goto 99
        gf3_rc = gft_read_full(jzname,    itime,  read_shape, &
        &     read_cnames, read_rank, read_time, read_coords, Jz)            
       if (gf3_rc .ne.1) goto 99

        print*, "time=", itime, read_time
        time = read_time
         
        rpsi = rpsi*factor
        ipsi = ipsi*factor
         	 	 
        temp(1,:) = rpsi(:)*Y2m2(1,:) - ipsi(:)*Y2m2(2,:)
        temp(2,:) = rpsi(:)*Y2m2(2,:) + ipsi(:)*Y2m2(1,:)
        c2m2(1,itime) = surface_integral2(temp(1,:),MAXMEM,Ndim)
        c2m2(2,itime) = surface_integral2(temp(2,:),MAXMEM,Ndim)

        temp(1,:) = rpsi(:)*Y2m1(1,:) - ipsi(:)*Y2m1(2,:)
        temp(2,:) = rpsi(:)*Y2m1(2,:) + ipsi(:)*Y2m1(1,:)
        c2m1(1,itime) = surface_integral2(temp(1,:),MAXMEM,Ndim)
        c2m1(2,itime) = surface_integral2(temp(2,:),MAXMEM,Ndim)

        temp(1,:) = rpsi(:)*Y20(1,:) - ipsi(:)*Y20(2,:)
        temp(2,:) = rpsi(:)*Y20(2,:) + ipsi(:)*Y20(1,:)
        c20(1,itime) = surface_integral2(temp(1,:),MAXMEM,Ndim)
        c20(2,itime) = surface_integral2(temp(2,:),MAXMEM,Ndim)

        temp(1,:) = rpsi(:)*Y2p1(1,:) - ipsi(:)*Y2p1(2,:)
        temp(2,:) = rpsi(:)*Y2p1(2,:) + ipsi(:)*Y2p1(1,:)
        c2p1(1,itime) = surface_integral2(temp(1,:),MAXMEM,Ndim)
        c2p1(2,itime) = surface_integral2(temp(2,:),MAXMEM,Ndim)

        temp(1,:) = rpsi(:)*Y2p2(1,:) - ipsi(:)*Y2p2(2,:)
        temp(2,:) = rpsi(:)*Y2p2(2,:) + ipsi(:)*Y2p2(1,:)
        c2p2(1,itime) = surface_integral2(temp(1,:),MAXMEM,Ndim)
        c2p2(2,itime) = surface_integral2(temp(2,:),MAXMEM,Ndim)

        tmass(itime) = surface_integral2(mass(:),MAXMEM,Ndim)
        tJz(itime)   = surface_integral2(Jz(:),MAXMEM,Ndim)

        !if z = x + iy we define ϕ=Arg(z) as
        ! arctan(y/x) when x > 0
        ! arctan(y/x)+pi when x < 0 and y ≥ 0 
        ! arctan(y/x)-pi when x < 0 and y < 0 
        ! pi/2 when x = 0 and y > 0
        ! -pi/2 when x = 0 and y < 0 
        !indeterminate when x = 0 and y = 0.

        smallnumber =1e-6
        x = c2p2(1,itime)
        y = c2p2(2,itime)
        if (x .gt. smallnumber) then
          phase(itime) = atan(y/x) 
        else if ((x .lt. -smallnumber) .AND. (y .ge. 0.0)) then 
          phase(itime) = atan(y/x)+pi
        else if ((x .lt. -smallnumber) .AND. (y .lt. 0.0)) then 
          phase(itime) = atan(y/x)-pi
        else if ((abs(x) .lt. smallnumber) .AND. (y .gt. 0.0)) then 
          phase(itime) = 0.5*pi
        else if ((abs(x) .lt. smallnumber) .AND. (y .lt. 0.0)) then 
          phase(itime) = -0.5*pi
        end if

        !----output on the screen ---------------------------
        if (output .EQ. 1) then
  	  print*, "-------------------------"      !
          print*, "-----MASS--------------"
          print*, " mass=", tmass(itime)
	  print*, "-------------------------"      
          print*, "-----ANGULAR MOMENTUM----"
          print*, " Jz=", tJz(itime)		
	  print*, "-------------------------"      
          print*, "-----L=2 MODES ----------"
  	  print*, "-------------------------"
	  print*, "c2m2=",c2m2(1,itime),"c2m2=",c2m2(2,itime)
	  print*, "c2m1=",c2m1(1,itime),"c2m1=",c2m1(2,itime)
	  print*, "c20=",c20(1,itime),  "c20=",c20(2,itime)
	  print*, "c2p1=",c2p1(1,itime),"c2p1=",c2p1(2,itime)
	  print*, "c2p2=",c2p2(1,itime),"c2p2=",c2p2(2,itime)
        end if	

        write(110,100) time, c2m2(1,itime), c2m2(2,itime) 
        write(111,100) time, c2m1(1,itime), c2m1(2,itime) 
        write(112,100) time, c20(1,itime), c20(2,itime) 
        write(113,100) time, c2p1(1,itime), c2p1(2,itime) 
        write(114,100) time, c2p2(1,itime), c2p2(2,itime) 

        write(120,100) time, tmass(itime)
        write(121,100) time, tJz(itime)
        write(122,100) time, phase(itime)


      end do

                  
 99   continue


      !-computing the frequency from the dominant mode l=m=2
      !-either by omega=dphase/dt
      ! or omega = -1/m*Im [ (d C_{lm}/dt)/ C_{lm}] 
      ! (a+i*b)/(c+i*d) = (a*c + b*d + i*(b*c - a*d))/(c^2 + d^2)

      do i=2, itime-2

         time = (i-1)*dt
         omega1(i) = 0.5*( phase(i+1)-phase(i-1) )/(2.0*dt)

         cR2p2   = c2p2(1,i)
         cI2p2   = c2p2(2,i)
         dtcR2p2 = (c2p2(1,i+1) - c2p2(1,i-1))/(2.0*dt)
         dtcI2p2 = (c2p2(2,i+1) - c2p2(2,i-1))/(2.0*dt)
         temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
         omega2(i) = abs((1.0d0/2.0d0)*temp1)
         write(130,100) time, omega2(i),omega1(i)
      end do


      !-nakano extrapolation to infinity
      ! first define some constant
      ra        = rext * ( 1.0 + massI/(2.0*rext) )**2
      rr        = rext * ( 1.0 + 2.0*massI/rext )**2
      factor    = 4.0/(2.0*ra) 
      integralc = 0.0
      print*, "ra=",ra,"factor", factor 

      do i=2, itime
        time = (i-1)*dt
        time_tort = time - (rr + 2.0*massI*log(0.5*rr/massI -1.0))
        if (time .GT. time_min) then
!          integralc = integralc + c2p2(:,i)*dt 
          integralc = integralc + c20(:,i)*dt 
        end if
!        c2p2_inf(:,i) = (1.0 - 2.0*massI/ra)* (c2p2(:,i) - factor*integralc)
        c2p2_inf(:,i) = (1.0 - 2.0*massI/ra)* (c20(:,i) - factor*integralc)
        write(131,100) time_tort, c2p2_inf(1,i), c2p2_inf(2,i), time
      end do 


      ! compute the GW luminosity and total energy radiated in GW
      ! first define some constant
      factor    = 1.0/(16.0*pi) 
      d2p2 = 0.0
      d2p1 = 0.0
      d20  = 0.0
      d2m1 = 0.0
      d2m2 = 0.0
      d2p2_inf = 0.0
      d2p1_inf = 0.0
      d20_inf  = 0.0
      d2m1_inf = 0.0
      d2m2_inf = 0.0
      print*, "ra=",ra,"factor", factor 

      do i=2, itime
        time = (i-1)*dt
        time_tort = time - (rr + 2.0*massI*log(0.5*rr/massI -1.0))
        ! with the raw data and the infinity extrapolation
        if (time .GT. time_min) then
          d2p2 = d2p2 + c2p2(:,i)*dt 
          d2p1 = d2p1 + c2p1(:,i)*dt 
          d20  = d20  + c20(:,i)*dt 
          d2m1 = d2m1 + c2m1(:,i)*dt 
          d2m2 = d2m2 + c2m2(:,i)*dt 

          d2p2_inf = d2p2_inf + c2p2_inf(:,i)*dt 
          d2p1_inf = d2p1_inf + c2p1_inf(:,i)*dt 
          d20_inf  = d20_inf  + c20_inf(:,i)*dt 
          d2m1_inf = d2m1_inf + c2m1_inf(:,i)*dt 
          d2m2_inf = d2m2_inf + c2m2_inf(:,i)*dt 
        end if

        LGW(i) = factor*( d2p2(1)**2 + d2p2(2)**2 +&
     &           d2p1(1)**2 + d2p1(2)**2 +&
     &           d20(1)**2 + d20(2)**2 +&
     &           d2m1(1)**2 + d2m1(2)**2 +&
     &           d2m2(1)**2 + d2m2(2)**2 )
    
        LGW_inf(i) = factor * (d2p2_inf(1)**2 + d2p2_inf(2)**2 +&
     &               d2p1_inf(1)**2 + d2p1_inf(2)**2 +&
     &               d20_inf(1)**2 + d20_inf(2)**2 +&
     &               d2m1_inf(1)**2 + d2m1_inf(2)**2 +&
     &               d2m2_inf(1)**2 + d2m2_inf(2)**2 )

        write(132,100) time_tort, LGW(i), LGW_inf(i), time
!        write(132,100) time, d20(1), d20(2), LGW(i)
      end do 


      ! integrate twice in time to get the strain
      d2p2T = 0.0
      d2p2T_inf = 0.0

      ! first time integral 
      do i=2, itime
        time = (i-1)*dt
        ! with the raw data and the infinity extrapolation
        if (time .GT. time_min) then
          d2p2T(:,i) = d2p2T(:,i-1) &
     &               + 0.5*dt*(c2p2(:,i-1) + c2p2(:,i))
          d2p2T_inf(:,i) = d2p2T_inf(:,i-1) &
     &                   + 0.5*dt*(c2p2_inf(:,i-1) + c2p2_inf(:,i))
        end if

        write(133,100) time, d2p2T(1,i), d2p2T(2,i), time_tort
        write(134,100) time_tort, d2p2T_inf(1,i), d2p2T_inf(2,i), time
      end do 

      h2p2 = 0.0
      h2p2_inf = 0.0
      ! second time integral 
      do i=2, itime
        time = (i-1)*dt
        time_tort = time - (rr + 2.0*massI*log(0.5*rr/massI -1.0))
        ! with the raw data and the infinity extrapolation
        if (time .GT. time_min) then
          h2p2(:,i) = h2p2(:,i-1) &
     &               + 0.5*dt*(d2p2T(:,i-1) + d2p2T(:,i))
          h2p2_inf(:,i) = h2p2_inf(:,i-1) &
     &                   + 0.5*dt*(d2p2T_inf(:,i-1) + d2p2T_inf(:,i))
        end if

        write(135,100) time, h2p2(1,i), h2p2(2,i), time_tort
!       C=0.22
!        write(136,100) time, h2p2(1,i) - 0.0144*(time-90), &
!     &                       h2p2(2,i) + 0.0005*(time-90) + 0.24, time_tort

!      C=0.18
!       fpR = -1.10106 + 0.0117589*time + 2.3293e-6*time*time
!       fpI = -0.147869 - 0.000422167*time + 1.08879e-6*time*time
!        write(136,100) time, h2p2(1,i) - fpR, &
!     &                       h2p2(2,i) - fpI, time_tort

!      C=0.12        
!       fpR = -1.1247 + 0.0117561*time - 5.22838e-6*time*time
!       fpI = -0.0096004 - 0.00184655*time + 1.678e-6*time*time
!        write(136,100) time, h2p2(1,i) - fpR, &
!     &                       h2p2(2,i) - fpI, time_tort

!      C=0.06
       fpR = 0.12023 - 0.000860982*time + 8.18843e-07*time*time
       fpI = -0.457471 + 0.00267428*time - 2.43701e-06*time*time
        write(136,100) time, h2p2(1,i) - fpR, &
     &                       h2p2(2,i) - fpI, time_tort


      end do 



      close(unit=110)
      close(unit=111)
      close(unit=112)
      close(unit=113)
      close(unit=114)

      close(unit=120)
      close(unit=121)
      close(unit=122)

      close(unit=130)
      close(unit=131)                 
      close(unit=132)                 
      close(unit=133)                 
      close(unit=134)                 
      close(unit=135)                 
      close(unit=136)                                              

!-----------------------------------------
      
  100 format(5e16.7)      

      end program

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!-----the integral on a sphere-------------------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!---------------------------------------------------
!-----basic integral on a sphere--------------------
!---------------------------------------------------

      real*8 function surface_integral(u,MAXMEM,N)     
      
      integer :: N, iphi,itheta,MAXMEM
      real*8  :: u(MAXMEM)
      real*8  :: phi,theta,pi,integral

      pi     = dacos(-1.0d0)
      h      = pi/N
      h2     = h*h
           
      do iphi = 1, 2*N+1
        phi = (iphi-1)*h
        do itheta = 1, N+1
          theta = (itheta-1)*h   
          index = (itheta-1)*(2*N+1) + iphi

	  integral = integral + h2*dsin(theta)*u(index)

      end do
      end do      	

      surface_integral = integral

      return 
      end function surface_integral

!---------------------------------------------------
!-----better integral on a sphere-------------------
!---------------------------------------------------

      real*8 function surface_integral2(u,MAXMEM,N)     
      
      integer :: N, iphi,itheta,MAXMEM
      real*8  :: u(MAXMEM)
      real*8  :: phi,theta,pi,integral

      pi       = dacos(-1.0d0)
      h        = pi/N
      h2       = h*h
      integral = 0.0
  
      ! corner points
      iphi = 1
      itheta = 1
      phi = (iphi-1)*h
      theta = (itheta-1)*h   
      index = (itheta-1)*(2*N+1) + iphi
      integral = integral + dsin(theta)*u(index)
     
      iphi = 2*N+1
      itheta = N+1
      phi = (iphi-1)*h
      theta = (itheta-1)*h   
      index = (itheta-1)*(2*N+1) + iphi
      integral = integral + dsin(theta)*u(index)

      iphi = 1
      itheta = N+1
      phi = (iphi-1)*h
      theta = (itheta-1)*h   
      index = (itheta-1)*(2*N+1) + iphi
      integral = integral + dsin(theta)*u(index)

      iphi = 2*N+1
      itheta = 1
      phi = (iphi-1)*h
      theta = (itheta-1)*h   
      index = (itheta-1)*(2*N+1) + iphi
      integral = integral + dsin(theta)*u(index)
      
      ! interior points
      do iphi = 2, 2*N
        phi = (iphi-1)*h
        do itheta = 2, N
          theta = (itheta-1)*h   
          index = (itheta-1)*(2*N+1) + iphi
	  integral = integral + 4.0*dsin(theta)*u(index)
      end do
      end do      	


      ! boundary points
      iphi = 1
      phi = (iphi-1)*h
      do itheta = 2, N
          theta = (itheta-1)*h   
          index = (itheta-1)*(2*N+1) + iphi
	  integral = integral + 2.0*dsin(theta)*u(index)
      end do

      iphi = 2*N+1
      phi = (iphi-1)*h
      do itheta = 2, N
        theta = (itheta-1)*h   
        index = (itheta-1)*(2*N+1) + iphi
	integral = integral + 2.0*dsin(theta)*u(index)
      end do

     itheta = 1
     theta = (itheta-1)*h    
      do iphi = 2, 2*N
        phi = (iphi-1)*h
        index = (itheta-1)*(2*N+1) + iphi
	integral = integral + 2.0*dsin(theta)*u(index)
      end do      	

     itheta = N+1
     theta = (itheta-1)*h    
      do iphi = 2, 2*N
        phi = (iphi-1)*h
        index = (itheta-1)*(2*N+1) + iphi
	integral = integral + 2.0*dsin(theta)*u(index)
      end do      	

      surface_integral2 = 0.25*h2*integral
      return 

      end function surface_integral2


