from scipy import integrate
import matplotlib.pyplot as pl
import numpy as np
from scipy.optimize import curve_fit

Rext=50
tm1= 950.
tm2= 300.
tm3= 335.
tm4= 218.

Mi1= 1.07
Mi2= 1.16
Mi3= 1.27
Mi4= 1.43


#### C006
dataset = np.genfromtxt(fname='../simulations/DS/C006/sphere50/lum_gw.dat')
t_mv       = dataset[:,0] 
dEdt       = dataset[:,1]
t006DS     = (dataset[:,2] - Rext - tm1)/Mi1
E_int006DS = integrate.simps(dEdt,t006DS)
E_cum006DS = integrate.cumtrapz(dEdt, t006DS, initial=0)
print 'C006 DBS'
print E_int006DS
print E_int006DS/1.07
np.savetxt('c006_ener', np.c_[t006DS,E_cum006DS] )



dataset = np.genfromtxt(fname='../simulations/C006/lum_gw.dat')
crap       = dataset[:,0] 
dEdt       = dataset[:,1]
crap       = dataset[:,2]
t006BS     = (dataset[:,3] - Rext - tm1)/Mi1
E_int006BS = integrate.simps(dEdt,t006BS)
E_cum006BS = integrate.cumtrapz(dEdt, t006BS, initial=0)
print 'C006 BS'
print E_int006BS
print E_int006BS/1.07


#### C012
dataset = np.genfromtxt(fname='../simulations/DS/C012/sphere50/lum_gw.dat')
t_mv        = dataset[:,0] 
dEdt        = dataset[:,1]
t012DS      = (dataset[:,2] - Rext - tm2)/Mi2
E_int012DS  = integrate.simps(dEdt,t012DS)
E_cum012DS  = integrate.cumtrapz(dEdt, t012DS, initial=0)
print 'C012 DBS'
print E_int012DS
print E_int012DS/1.18

np.savetxt('c012_ener', np.c_[t012DS,E_cum012DS] )


dataset = np.genfromtxt(fname='../simulations/C012/lum_gw.dat')
crap       = dataset[:,0] 
dEdt       = dataset[:,1]
crap       = dataset[:,2]
t012BS     = (dataset[:,3] - Rext - tm2)/Mi2
E_int012BS = integrate.simps(dEdt,t012BS)
E_cum012BS = integrate.cumtrapz(dEdt, t012BS, initial=0)
print 'C012 BS'
print E_int012BS
print E_int012BS/1.18


#### C018
dataset    = np.genfromtxt(fname='../simulations/DS/C018_new/sphere50/lum_gw.dat')
t_mv       = dataset[:,0]
dEdt       = dataset[:,1]
t018DS     = (dataset[:,2] - Rext - tm3)/Mi3
E_int018DS = integrate.simps(dEdt,t018DS)
E_cum018DS = integrate.cumtrapz(dEdt, t018DS, initial=0)
print 'C018 DBS'
print E_int018DS
print E_int018DS/1.29


dataset = np.genfromtxt(fname='../simulations/C018/lum_gw.dat')
crap = dataset[:,0] 
dEdt       = dataset[:,1]
crap       = dataset[:,2]
t018BS     = (dataset[:,3] - Rext - tm3)/Mi3
E_int018BS = integrate.simps(dEdt,t018BS)
E_cum018BS = integrate.cumtrapz(dEdt, t018BS, initial=0)
print 'C018 BS'
print E_int018BS
print E_int018BS/1.29


#### C022
dataset = np.genfromtxt(fname='../simulations/DS/C022/sphere50/lum_gw.dat')
t_mv       = dataset[:,0] 
dEdt       = dataset[:,1]
t022DS     = (dataset[:,2] - Rext - tm4)/Mi4
E_int022DS = integrate.simps(dEdt,t022DS)
E_cum022DS = integrate.cumtrapz(dEdt, t022DS, initial=0)
print 'C022 DBS'
print E_int022DS
print E_int022DS/1.46


dataset = np.genfromtxt(fname='../simulations/C022/lum_gw.dat')
crap       = dataset[:,0] 
dEdt       = dataset[:,1]
crap       = dataset[:,2]
t022BS     = (dataset[:,3] - Rext - tm4)/Mi4
E_int022BS = integrate.simps(dEdt,t022BS)
E_cum022BS = integrate.cumtrapz(dEdt, t022BS, initial=0)
print 'C022 BS'
print E_int022BS
print E_int022BS/1.46


# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=False)

axarr[0].plot(t006DS, E_cum006DS/Mi1, 'r',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(t006BS, E_cum006BS/Mi1, 'r--',linewidth = 1,label=r'$C=0.06$ BS')
axarr[0].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[0].legend(handles=[l1],loc='4', labelspacing=0.2,prop={'size':12},ncol=2)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=15)
axarr[0].tick_params(axis='x', which='major',  labelsize=15)
axarr[0].set_xlim([-195.0,1000.0])
#axarr[0].set_ylim([-0.009,0.013])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$E_{rad}/M_{0}$',fontsize=20)


axarr[1].plot(t012DS, E_cum012DS/Mi2, 'b',linewidth = 3,label=r'$C=0.12$ DBS')
axarr[1].plot(t012BS, E_cum012BS/Mi2, 'b--',linewidth = 1,label=r'$C=0.12$ BS')
axarr[1].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[1].set_xlabel('t',fontsize=20)
axarr[1].tick_params(axis='y', which='major',  labelsize=15)
axarr[1].tick_params(axis='x', which='major',  labelsize=15)
axarr[1].set_xlim([-198.0,580.0])
#axarr[1].set_ylim([0.0,0.03])    
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[1].set_ylabel(r'$E_{rad}/M_{0}$',fontsize=20)

axarr[2].plot(t018DS + 90, E_cum018DS/Mi3, 'k',linewidth = 3,label=r'$C=0.18$ DBS')
axarr[2].plot(t018BS + 90, E_cum018BS/Mi3, 'k--',linewidth = 1,label=r'$C=0.18$ BS')
axarr[2].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
axarr[2].set_xlabel('t',fontsize=20)
axarr[2].tick_params(axis='y', which='major',  labelsize=15)
axarr[2].tick_params(axis='x', which='major',  labelsize=15)
axarr[2].set_xlim([-198.0,180.0])
#axarr[2].set_ylim([-0.044,0.057])    
for label in axarr[2].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[2].set_ylabel(r'$E_{rad}/M_{0}$',fontsize=20)


axarr[3].plot(t022DS, E_cum022DS/Mi4, 'g',linewidth = 3,label=r'$C=0.22$ DBS')
axarr[3].plot(t022BS, E_cum022BS/Mi4, 'g--',linewidth = 1,label=r'$C=0.22$ BS')
axarr[3].legend(loc='upper left', labelspacing=0.1,prop={'size':12},ncol=1)
#axarr[3].set_xlabel('t',fontsize=20)
axarr[3].tick_params(axis='y', which='major',  labelsize=15)
axarr[3].tick_params(axis='x', which='major',  labelsize=15)
axarr[3].set_xlim([-145.0,150.0])
#axarr[3].set_ylim([-0.045,0.057])    
for label in axarr[3].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[3].set_ylabel(r'$E_{rad}/M_{0}$',fontsize=20)
axarr[3].set_xlabel(r'$(t - t_{c})/M_0$',fontsize=20)



pl.tight_layout(pad=0.1)
pl.subplots_adjust(wspace=0.5, hspace=0.3)

#pl.tight_layout()

pl.draw()

pl.savefig("energy_int.pdf",bbox_inches="tight")
#pl.show()










