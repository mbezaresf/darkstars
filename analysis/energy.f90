
      program      main
      implicit     none

        
      integer :: i, j, k
      
      integer Ndim,DN,NF
!      parameter ( Ndim =  1974 )
!      parameter ( Ndim =  681 )
!      parameter ( Ndim =  666 )
      parameter ( Ndim =  736 )
                          
      real*8                  ::  MM0,MMF,MMP,MMIP,MM,dt,EE
      real*8, dimension(Ndim) ::  t_mv,dEdt,t

      
      open(unit=111, file="/home/miguel/Programs/papers/darkstars/simulations/DS/C022/sphere50/lum_gw.dat")
      do i=1,  Ndim
         read(111,*) t_mv(i),dEdt(i),t(i)
      end do

print*,t_mv(Ndim),dEdt(Ndim),t(Ndim)
!Integral, using Simpson Composed Rule, the interval for integral is [R0, NF]

       DN  =  0.5*Ndim
       NF  =  1.0*Ndim

       MM0 = dEdt(1)
       MMF = dEdt(NF)
       
      
       do k= 1,DN-1

       MMP = dEdt(2*k)+MMP

       end do
        

       do k= 1,DN

       MMIP = dEdt(2*k-1)+MMIP

       end do

!MASS TOTAL between R0 and RM
       dt = t(2) - t(1)
       EE = (dt/3.0)*(MM0+2.0*MMP+4.0*MMIP+MMF)

!print*,dt,EE/1.07
!print*,dt,EE/1.18
!print*,dt,EE/1.29
print*,dt,EE/1.46
      end program


