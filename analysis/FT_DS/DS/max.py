#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *

dataset = np.genfromtxt(fname='norm1_ds')
t1 = dataset[:,0] 
x1 = dataset[:,1] 


dataset = np.genfromtxt(fname='norm2_ds')
t2 = dataset[:,0] 
x2 = dataset[:,1] 

dataset = np.genfromtxt(fname='norm3_ds')
t3 = dataset[:,0] 
x3 = dataset[:,1] 

dataset = np.genfromtxt(fname='norm4_ds')
t4 = dataset[:,0] 
x4 = dataset[:,1] 


print "Max value element norm : ", max(x1),max(x2),max(x3),max(x4)
print "Max value element t : ", t1[x1.argmax()],t2[x2.argmax()],t3[x3.argmax()],t4[x4.argmax()]


dataset = np.genfromtxt(fname='./FT_realh22/FT_h22_c006_ds')
t1 = dataset[:,0] 
x1 = dataset[:,1] 

dataset = np.genfromtxt(fname='./FT_realh22/FT_h22_c012_ds')
t2 = dataset[:,0] 
x2 = dataset[:,1] 

dataset = np.genfromtxt(fname='./FT_realh22/FT_h22_c018_ds')
t3 = dataset[:,0] 
x3 = dataset[:,1] 

dataset = np.genfromtxt(fname='./FT_realh22/FT_h22_c022_ds')
t4 = dataset[:,0] 
x4 = dataset[:,1] 

print "Max value element h22  : ", max(x1),max(x2),max(x3),max(x4)
print "Max value element freq : ", t1[x1.argmax()],t2[x2.argmax()],t3[x3.argmax()],t4[x4.argmax()]

