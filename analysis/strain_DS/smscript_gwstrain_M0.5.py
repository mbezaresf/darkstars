#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d

##################################################
##     strain                                   ##
##################################################
def strain(t, x, y, cutoff):
   # Interpolate to uniform grid:
   tU     = np.linspace(t[0],max(t),len(t))
   jR     = interp1d(t,x)
   jI     = interp1d(t,y)
   xU     = jR(tU)
   yU     = jI(tU)
   zU     = xU+1j*yU
   # Compute the FFT:
   fftU   = fft(zU)
   tstep  = tU[1]-tU[0]
   N      = zU.size
   # Compute the frequencies of the FFT:
   fftU_f = fftfreq(N,d=tstep)
   # Kill the DC offset:
   fftU[0]= 0.0
   # Change the low-end frequencies
   freqs=fftU_f
   # Make sure zero frequency does not cause error
   freqs[0]=1.0
   for i in range(len(fftU_f)):
      if (abs(fftU_f[i]) < cutoff):
         freqs[i] = cutoff*sign(fftU_f[i])
      else:
         freqs[i] = fftU_f[i]
   # Do the integration and inverse FFT:
   strain = -ifft(fftU/((2.0*pi*freqs)**2))
   return tU, strain

M1= 1.0
M2= 1.0
M3= 1.0
M4= 1.0

tm1_ds= 2427.
tm2_ds= 633.
tm3_ds= 313.
tm4_ds= 288.


#M1= 1.07
#M2= 1.16
#M3= 1.27
#M4= 1.43
################# C006 #################################
###### Strain Solitonic Stars
#dataset = np.genfromtxt(fname='../../simulations/C006/c2p2B.dat')
#t1=dataset[:,0]
#x1=dataset[:,1]/1.07 
#y1=dataset[:,2]/1.07 
#----------------
#tf1, strainf1 = strain(t1, x1, y1, 0.006)
#xf1           = strainf1.real

#np.savetxt('h22R_C006_SBS.out', np.c_[tf1,xf1] )   # x,y,z equal sized 1D arrays

###### Strain Dark Stars
#dataset = np.genfromtxt(fname='../../simulations/DS/C006/sphere50/c2p2.dat')
#tt1_ds=dataset[:,0]
#x1_ds=dataset[:,1]/M1 
#y1_ds=dataset[:,2]/M1 

#ttf1_ds, strain1f1_ds = strain(tt1_ds, x1_ds, y1_ds, 0.006)
#xff1_ds         = strain1f1_ds.real
#i_xff1_ds       = strain1f1_ds.imag

#np.savetxt('h22R_C006_DS.out', np.c_[ttf1_ds,xff1_ds] )

###### h22 tidal
#dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidal/h22.dat')
#t1_006 = dataset[:,0] 
#h22_006   = dataset[:,1]

###### h22 no tidal
#dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidalzero/h22.dat')
#t1_n_006  = dataset[:,0] 
#h22_n_006 = dataset[:,1]

#### Sepa tidal 
#dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidal/t_omega_r.dat')
#t1_sep_006  = dataset[:,0] 
#Momega_006  = dataset[:,1]
#sepa_006   = dataset[:,2]

#### Sepa no tidal 
#dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidalzero/t_omega_r.dat')
#t1_sep_n006  = dataset[:,0] 
#Momega_n006  = dataset[:,1]
#sepa_n006   = dataset[:,2]


# Two subplots sharing x axes
#f, axarr = pl.subplots(2, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()

#axarr[0].plot(tf1,xf1, 'k',linewidth = 1,label=r'$C=0.06$ SBS NR')
#axarr[0].plot(ttf1_ds,xff1_ds, 'r--',linewidth = 1,label=r'$C=0.06$ DS NR')
#axarr[0].plot(t1_006+70, 1.77*h22_006, 'b--',linewidth = 1,label=r'$C=0.06$ PN $\kappa_{2}=8420$')
#axarr[0].plot(t1_n_006+70, 1.77*h22_n_006, 'g--',linewidth = 1,label=r'$C=0.06$ PN $\kappa_{2}=0$')
#axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':10},ncol=4)
#axarr[0].set_xlabel('t',fontsize=20)
#axarr[0].tick_params(axis='y', which='major',  labelsize=12)
#axarr[0].tick_params(axis='x', which='major',  labelsize=12)
#axarr[0].set_xlim([200.0,1000])
#axarr[0].set_ylim([-0.2,0.2])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)

#axarr[1].plot(t1_sep_006,sepa_006, 'b--',linewidth = 1,label=r'$C=0.06$ PN $\kappa_{2}=8420$')
#axarr[1].plot(t1_sep_n006,sepa_n006, 'g--',linewidth = 1,label=r'$C=0.06$ PN $\kappa_{2}=0$')
#axarr[1].plot(t1_006+70, 1.77*h22_006, 'b--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=8420$')
#axarr[1].plot(t1_n_006+70, 1.77*h22_n_006, 'g--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=0$')
#axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':12},ncol=1)
#axarr[1].set_xlabel('t',fontsize=20)
#axarr[1].tick_params(axis='y', which='major',  labelsize=12)
#axarr[1].tick_params(axis='x', which='major',  labelsize=12)
#axarr[1].set_xlim([200.0,1000])
#axarr[1].set_ylim([6,17])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)


#pl.tight_layout(pad=0.1)
#plt.subplots_adjust(wspace=0.5, hspace=0.2)
#pl.tight_layout()

#pl.draw()

#pl.savefig("orbital_c006.pdf")




################# C012 #################################
###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C012/c2p2B.dat')
t2=dataset[:,0]
x2=dataset[:,1]
y2=dataset[:,2]
#----------------
tf2, strainf2 = strain(t2, x2, y2, 0.006)
xf2           = strainf2.real
i_xf2         = strainf2.imag

np.savetxt('h22R_C012_SBS.out', np.c_[tf2,xf2] )   # x,y,z equal sized 1D arrays

###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C012/sphere50/c2p2.dat')
tt2_ds=dataset[:,0]
x2_ds=dataset[:,1]
y2_ds=dataset[:,2]

#----------------
ttf2_ds, strain2f2_ds = strain(tt2_ds, x2_ds, y2_ds, 0.006)
xff2_ds         = strain2f2_ds.real
i_xff2_ds       = strain2f2_ds.imag

np.savetxt('h22R_C012_DS.out', np.c_[ttf2_ds,xff2_ds] )   # x,y,z equal sized 1D arrays

###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidal/h22.dat')
t1_012  = dataset[:,0] 
h22_012 = dataset[:,1]

###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidalzero/h22.dat')
t1_n_012  = dataset[:,0] 
h22_n_012 = dataset[:,1]

#### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidal/t_omega_r.dat')
t1_sep_012  = dataset[:,0] 
Momega_012  = dataset[:,1]
sepa_012   = dataset[:,2]

#### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidalzero/t_omega_r.dat')
t1_sep_n012  = dataset[:,0] 
Momega_n012  = dataset[:,1]
sepa_n012   = dataset[:,2]

#SBS star
f130 = open('omega_012_SB.dat', 'w')
dt   = 1.996007984031936133e-01
for i in xrange(1, len(tf2) - 1):
        time = i*dt
        cR2p2   = xf2[i]
        cI2p2   = i_xf2[i]
	dtcR2p2 = (xf2[i+1] - xf2[i-1]) / (2.0*dt)
        dtcI2p2 = (i_xf2[i+1] - i_xf2[i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_012_SB.dat')
tomg_sbs      = dataset[:,0]/1.18
omega_012_sbs = 1.18*dataset[:,1]

np.savetxt('omega_012_SB.dat', np.c_[tomg_sbs,omega_012_sbs] )


## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 250
omg_av_sbs = movingaverage(omega_012_sbs, r1)

np.savetxt('omega_av_SBS.dat', np.c_[tomg_sbs,omg_av_sbs] )



#Dark star
f130 = open('omega_012_1.dat', 'w')
dt   = 1.5
for i in xrange(1, len(tt2_ds) - 1):
        time = i*dt
        cR2p2   = xff2_ds[i]
        cI2p2   = i_xff2_ds[i]
	dtcR2p2 = (xff2_ds[i+1] - xff2_ds[i-1]) / (2.0*dt)
        dtcI2p2 = (i_xff2_ds[i+1] - i_xff2_ds[i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_012_1.dat')
tomg      = dataset[:,0]/1.18
omega_012 = 1.18*dataset[:,1]

np.savetxt('omega_012.dat', np.c_[tomg,omega_012] )


## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 50
omg_av = movingaverage(omega_012, r1)

np.savetxt('omega_av.dat', np.c_[tomg,omg_av] )


#print np.prod(omg_av_sbs.shape)
#print omg_av_sbs[0:omg_av_sbs.shape[0]-300]
#print omg_av_sbs[0:omg_av_sbs.shape[0]-300].shape


Rext = 50.
tm2  = 300.
Mi2  = 1.16

tm4 = 218.
Mi4 = 1.43

dataset = np.genfromtxt(fname='../../simulations/C012/JzB.dat')
tt2=(dataset[:,0] - Rext - tm2)/Mi2
j2=dataset[:,1] 

r2 = 10
j2_av = movingaverage(j2, r2)

dataset = np.genfromtxt(fname='../../simulations/DS/C012/sphere50/Jz.dat')
tt2_ds=(dataset[:,0] - Rext - tm2)/Mi2
j2_ds=dataset[:,1] 

r2_ds = 1
j2_av_ds = movingaverage(j2_ds, r2_ds)

dataset = np.genfromtxt(fname='../../simulations/DS/C022/sphere50/Jz.dat')
tt4_ds=(dataset[:,0] - Rext - tm4)/Mi4
j4_ds=dataset[:,1] 

r4_ds = 1
j4_av_ds = movingaverage(j4_ds, r4_ds)

dataset = np.genfromtxt(fname='../../simulations/C022new/JzB.dat')
tt4=(dataset[:,0] - Rext - tm4)/Mi4
j4=dataset[:,1] 

r4 = 1
j4_av = movingaverage(j4, r4)

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()

axarr[0].plot(tomg_sbs[0:tomg_sbs.shape[0]-300],omg_av_sbs[0:omg_av_sbs.shape[0]-300], 'b:',linewidth = 3,label=r'$C=0.12$ SB')
axarr[0].axvline(x=300-50,color='k',ls='--',linewidth = 1)
axarr[0].annotate(r' $t_{contact}$', xy=(300-50, 0.01), xytext=(340-50, 0.01), size=18,arrowprops=dict(arrowstyle="->",connectionstyle="arc3,rad=.2",color='black'))
axarr[0].plot(tomg,omg_av, 'b-',linewidth = 1,label=r'$C=0.12$ DSB')
axarr[0].plot(t1_sep_012-50,Momega_012, 'm-',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=332$')
axarr[0].plot(t1_sep_n012-140,Momega_n012, 'c-',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=0$')
#axarr[0].plot(t1_012-27, 1.7*h22_012, 'b--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=332$')
#axarr[0].plot(t1_n_012-27, 1.7*h22_n_012, 'g--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=0$')
axarr[0].legend(loc='lower right', labelspacing=0.1,prop={'size':10},ncol=2)
axarr[0].set_xlabel('t',fontsize=15)
axarr[0].tick_params(axis='y', which='major',  labelsize=12)
axarr[0].tick_params(axis='x', which='major',  labelsize=12)
axarr[0].set_xlim([131.0,600])
axarr[0].set_ylim([0.0,0.13])    
for label in axarr[0].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[0].set_ylabel(r'$f_{GW}$',fontsize=18)

axarr[1].plot(tomg_sbs[0:tomg_sbs.shape[0]-300],omg_av_sbs[0:omg_av_sbs.shape[0]-300], 'b:',linewidth = 3,label=r'$C=0.12$ SB')
axarr[1].axvline(x=300-50,color='k',ls='--',linewidth = 1)
axarr[1].annotate(r' $t_{contact}$', xy=(300-50, 0.01), xytext=(340-50, 0.01), size=18,arrowprops=dict(arrowstyle="->",connectionstyle="arc3,rad=.2",color='black'))
axarr[1].plot(tomg,omg_av, 'b-',linewidth = 1,label=r'$C=0.12$ DSB')
axarr[1].plot(t1_sep_012-50,Momega_012, 'm-',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=332$')
axarr[1].plot(t1_sep_n012-140,Momega_n012, 'c-',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=0$')
#axarr[0].plot(t1_012-27, 1.7*h22_012, 'b--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=332$')
#axarr[0].plot(t1_n_012-27, 1.7*h22_n_012, 'g--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=0$')
axarr[1].legend(loc='lower right', labelspacing=0.1,prop={'size':10},ncol=2)
axarr[1].set_xlabel('t',fontsize=15)
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
axarr[1].set_xlim([131.0,600])
axarr[1].set_ylim([0.0,0.13])    
for label in axarr[1].get_yticklabels()[1::2]:
    label.set_visible(False) 
axarr[1].set_ylabel(r'$f_{GW}$',fontsize=18)


pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.2)
#pl.tight_layout()

pl.draw()

pl.savefig("orbital_c012.pdf")





################# C018 #################################
###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C018new/c2p2B.dat')
t3=dataset[:,0]/1.33
x3=dataset[:,1]
y3=dataset[:,2]
#----------------
tf3, strainf3 = strain(t3, x3, y3, 0.016)
xf3           = strainf3.real

np.savetxt('h22R_C018_SBS.out', np.c_[tf3,xf3] )   # x,y,z equal sized 1D arrays

###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C018_new/sphere50/c2p2.dat')
tt3_ds= dataset[:,0]
t3_ds=(dataset[:,0] - tm3_ds)/M3
x3_ds=dataset[:,1]/M3 
y3_ds=dataset[:,2]/M3 
#----------------
tf3_ds, strainf3_ds = strain(t3_ds, x3_ds, y3_ds, 0.016)
xf3_ds              = strainf3_ds.real
#----------------
ttf3_ds, strain3f3_ds = strain(tt3_ds, x3_ds, y3_ds, 0.016)
xff3_ds               = strain3f3_ds.real

np.savetxt('h22R_C018_DS.out', np.c_[ttf3_ds,xff3_ds] )   # x,y,z equal sized 1D arrays

###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C018_new/PN_tidal/h22.dat')
t1_018 = dataset[:,0] 
h22_018   = dataset[:,1]

###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C018_new/PN_tidalzero/h22.dat')
t1_n_018  = dataset[:,0] 
h22_n_018 = dataset[:,1]

#### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C018_new/PN_tidal/t_omega_r.dat')
t1_sep_018  = dataset[:,0] 
Momega_018  = dataset[:,1]
sepa_018    = dataset[:,2]

#### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C018_new/PN_tidalzero/t_omega_r.dat')
t1_sep_n018  = dataset[:,0] 
Momega_n018  = dataset[:,1]
sepa_n018   = dataset[:,2]
    
# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()

axarr[0].plot(tf3,xf3, 'k',linewidth = 1,label=r'$C=0.18$ SBS NR')
axarr[0].plot(ttf3_ds,xff3_ds, 'r--',linewidth = 1,label=r'$C=0.18$ DS NR')
axarr[0].plot(t1_018-55, 0.5*h22_018, 'b--',linewidth = 1,label=r'$C=0.18$ PN $\kappa_{2}=332$')
axarr[0].plot(t1_n_018-55, 0.5*h22_n_018, 'g--',linewidth = 1,label=r'$C=0.18$ PN $\kappa_{2}=0$')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':10},ncol=4)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=12)
axarr[0].tick_params(axis='x', which='major',  labelsize=12)
axarr[0].set_xlim([50.0,220])
#axarr[0].set_ylim([-0.2,0.2])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)

axarr[1].plot(t1_sep_018,sepa_018, 'b--',linewidth = 1,label=r'$C=0.18$ PN $\kappa_{2}=332$')
axarr[1].plot(t1_sep_n018,sepa_n018, 'g--',linewidth = 1,label=r'$C=0.18$ PN $\kappa_{2}=0$')
#axarr[1].plot(t1_006+70, 1.77*h22_006, 'b--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=8420$')
#axarr[1].plot(t1_n_006+70, 1.77*h22_n_006, 'g--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=0$')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':12},ncol=1)
axarr[1].set_xlabel('t',fontsize=20)
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
#axarr[1].set_xlim([200.0,1000])
axarr[1].set_ylim([3,11])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)


pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.2)
#pl.tight_layout()

pl.draw()

pl.savefig("orbital_c018.pdf")


################# C022 #################################
###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C022/c2p2B.dat')
t4=dataset[:,0]
x4=dataset[:,1]/M1 
y4=dataset[:,2]/M1 
#----------------
tf4, strainf4 = strain(t4, x4, y4, 0.010)
xf4           = strainf4.real

np.savetxt('h22R_C022_SBS.out', np.c_[tf4,xf4] )   # x,y,z equal sized 1D arrays

###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C022/sphere50/c2p2.dat')
tt4_ds=dataset[:,0] 
x4_ds=dataset[:,1]/M4 
y4_ds=dataset[:,2]/M4 
#----------------
ttf4_ds, strain4f4_ds = strain(tt4_ds, x4_ds, y4_ds, 0.010)
xff4_ds               = strain4f4_ds.real

np.savetxt('h22R_C022_DS.out', np.c_[ttf4_ds,xff4_ds] )   # x,y,z equal sized 1D arrays


###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidal/h22.dat')
t1_022 = dataset[:,0] 
h22_022   = dataset[:,1]

###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidalzero/h22.dat')
t1_n_022  = dataset[:,0] 
h22_n_022 = dataset[:,1]

#### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidal/t_omega_r.dat')
t1_sep_022  = dataset[:,0] 
Momega_022  = dataset[:,1]
sepa_022    = dataset[:,2]

#### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidalzero/t_omega_r.dat')
t1_sep_n022  = dataset[:,0] 
Momega_n022  = dataset[:,1]
sepa_n022   = dataset[:,2]



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=False)
#pl.xlim(10.0, 715.0)
#pl.ylim(-0.1, 10e-10 )
#pl.ylabel('separation (km)')
#axarr[0].pl.semilogy()

axarr[0].plot(tf4,xf4, 'k',linewidth = 1,label=r'$C=0.22$ SBS NR')
axarr[0].plot(ttf4_ds,xff4_ds, 'r--',linewidth = 1,label=r'$C=0.22$ DS NR')
axarr[0].plot(t1_022-30, 1.7*h22_022, 'b--',linewidth = 1,label=r'$C=0.22$ PN $\kappa_{2}=332$')
axarr[0].plot(t1_n_022-30, 1.7*h22_n_022, 'g--',linewidth = 1,label=r'$C=0.22$ PN $\kappa_{2}=0$')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':10},ncol=4)
#axarr[0].set_xlabel('t',fontsize=20)
axarr[0].tick_params(axis='y', which='major',  labelsize=12)
axarr[0].tick_params(axis='x', which='major',  labelsize=12)
axarr[0].set_xlim([60.0,220])
#axarr[0].set_ylim([-0.2,0.2])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)

axarr[1].plot(t1_sep_022,sepa_022, 'b--',linewidth = 1,label=r'$C=0.22$ PN $\kappa_{2}=332$')
axarr[1].plot(t1_sep_n022,sepa_n022, 'g--',linewidth = 1,label=r'$C=0.22$ PN $\kappa_{2}=0$')
#axarr[1].plot(t1_006+70, 1.77*h22_006, 'b--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=8420$')
#axarr[1].plot(t1_n_006+70, 1.77*h22_n_006, 'g--',linewidth = 2,label=r'$C=0.06$ PN $\kappa_{2}=0$')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':12},ncol=1)
axarr[1].set_xlabel('t',fontsize=20)
axarr[1].tick_params(axis='y', which='major',  labelsize=12)
axarr[1].tick_params(axis='x', which='major',  labelsize=12)
#axarr[1].set_xlim([200.0,1000])
axarr[1].set_ylim([3.1,11])    
#for label in axarr[0].get_yticklabels()[1::2]:
#    label.set_visible(False) 
#axarr[0].set_ylabel(r'$\Re(M\, \Psi^{2,2}_{4})$',fontsize=18)


pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.2)
#pl.tight_layout()

pl.draw()

pl.savefig("orbital_c022.pdf")




