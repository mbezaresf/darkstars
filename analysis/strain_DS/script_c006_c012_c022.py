#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
from pylab import *
from scipy.fftpack import fft
from scipy.interpolate import interp1d

##################################################
##     strain                                   ##
##################################################
def strain(t, x, y, cutoff):
   # Interpolate to uniform grid:
   tU     = np.linspace(t[0],max(t),len(t))
   jR     = interp1d(t,x)
   jI     = interp1d(t,y)
   xU     = jR(tU)
   yU     = jI(tU)
   zU     = xU+1j*yU
   # Compute the FFT:
   fftU   = fft(zU)
   tstep  = tU[1]-tU[0]
   N      = zU.size
   # Compute the frequencies of the FFT:
   fftU_f = fftfreq(N,d=tstep)
   # Kill the DC offset:
   fftU[0]= 0.0
   # Change the low-end frequencies
   freqs=fftU_f
   # Make sure zero frequency does not cause error
   freqs[0]=1.0
   for i in range(len(fftU_f)):
      if (abs(fftU_f[i]) < cutoff):
         freqs[i] = cutoff*sign(fftU_f[i])
      else:
         freqs[i] = fftU_f[i]
   # Do the integration and inverse FFT:
   strain = -ifft(fftU/((2.0*pi*freqs)**2))
   return tU, strain

M1= 1.0
M2= 1.0
M3= 1.0
M4= 1.0

tm1_ds= 2427.
tm2_ds= 633.
tm3_ds= 313.
tm4_ds= 288.

################# C012 #################################
###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C012/c2p2B.dat')
t2=dataset[:,0]
x2=dataset[:,1]
y2=dataset[:,2]
 #----------------
tf2, strainf2 = strain(t2, x2, y2, 0.006)
xf2           = strainf2.real
i_xf2         = strainf2.imag

#np.savetxt('h22R_C012_SBS.out', np.c_[tf2,xf2] )   # x,y,z equal sized 1D arrays

# ###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C012/sphere50/c2p2.dat')
tt2_ds=dataset[:,0]
x2_ds=dataset[:,1]
y2_ds=dataset[:,2]

# #----------------
ttf2_ds, strain2f2_ds = strain(tt2_ds, x2_ds, y2_ds, 0.006)
xff2_ds         = strain2f2_ds.real
i_xff2_ds       = strain2f2_ds.imag

# np.savetxt('h22R_C012_DS.out', np.c_[ttf2_ds,xff2_ds] )   # x,y,z equal sized 1D arrays

# ###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidal/h22.dat')
t1_012  = dataset[:,0] 
h22_012 = dataset[:,1]

# ###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidalzero/h22.dat')
t1_n_012  = dataset[:,0] 
h22_n_012 = dataset[:,1]

# #### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidal/t_omega_r.dat')
t1_sep_012  = dataset[:,0] 
Momega_012  = dataset[:,1]
sepa_012   = dataset[:,2]

# #### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidalzero/t_omega_r.dat')
t1_sep_n012  = dataset[:,0] 
Momega_n012  = dataset[:,1]
sepa_n012   = dataset[:,2]

# #SBS star
f130 = open('omega_012_SB.dat', 'w')
dt   = 1.996007984031936133e-01
for i in xrange(1, len(tf2) - 1):
        time = i*dt
        cR2p2   = xf2[i]
        cI2p2   = i_xf2[i]
	dtcR2p2 = (xf2[i+1] - xf2[i-1]) / (2.0*dt)
        dtcI2p2 = (i_xf2[i+1] - i_xf2[i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_012_SB.dat')
tomg_sbs      = dataset[:,0]/1.18
omega_012_sbs = 1.18*dataset[:,1]

# np.savetxt('omega_012_SB.dat', np.c_[tomg_sbs,omega_012_sbs] )


# ## Frequencia con h22
def movingaverage(interval, window_size):
     window = np.ones(int(window_size))/float(window_size)
     return np.convolve(interval, window, 'same')

r1 = 250
omg_av_sbs = movingaverage(omega_012_sbs, r1)

np.savetxt('omega_av_SBS.dat', np.c_[tomg_sbs,omg_av_sbs] )



# #Dark star
f130 = open('omega_012_1.dat', 'w')
dt   = 1.5
for i in xrange(1, len(tt2_ds) - 1):
         time = i*dt
         cR2p2   = xff2_ds[i]
         cI2p2   = i_xff2_ds[i]
  	 dtcR2p2 = (xff2_ds[i+1] - xff2_ds[i-1]) / (2.0*dt)
         dtcI2p2 = (i_xff2_ds[i+1] - i_xff2_ds[i-1]) / (2.0*dt)
         temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
         omega2 = abs((1.0/2.0)*temp1)
         f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_012_1.dat')
tomg      = dataset[:,0]/1.18
omega_012 = 1.18*dataset[:,1]

# np.savetxt('omega_012.dat', np.c_[tomg,omega_012] )


## Frequencia con h22
def movingaverage(interval, window_size):
     window = np.ones(int(window_size))/float(window_size)
     return np.convolve(interval, window, 'same')

r1 = 50
omg_av = movingaverage(omega_012, r1)

# np.savetxt('omega_av.dat', np.c_[tomg,omg_av] )



# ################# C022 #################################
# ###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C022new/c2p2B.dat')
t4=dataset[:,0]
x4=dataset[:,1]
y4=dataset[:,2]
#----------------
tf4, strainf4 = strain(t4, x4, y4, 0.010)
xf4           = strainf4.real
i_xf4         = strainf4.imag

# np.savetxt('h22R_C022_SBS.out', np.c_[tf4,xf4] )   # x,y,z equal sized 1D arrays

# ###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C022/sphere50/c2p2.dat')
tt4_ds=dataset[:,0]
x4_ds=dataset[:,1]
y4_ds=dataset[:,2]

# #----------------
ttf4_ds, strain4f4_ds = strain(tt4_ds, x4_ds, y4_ds, 0.010)
xff4_ds         = strain4f4_ds.real
i_xff4_ds       = strain4f4_ds.imag

# np.savetxt('h22R_C022_DS.out', np.c_[ttf4_ds,xff4_ds] )   # x,y,z equal sized 1D arrays

# ###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidal/h22.dat')
t1_022  = dataset[:,0] 
h22_022 = dataset[:,1]

# ###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidalzero/h22.dat')
t1_n_022  = dataset[:,0] 
h22_n_022 = dataset[:,1]

# #### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C022/PN_tidal/t_omega_r.dat')
t1_sep_022  = dataset[:,0] 
Momega_022  = dataset[:,1]
sepa_022   = dataset[:,2]

# #### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C012/PN_tidalzero/t_omega_r.dat')
t1_sep_n022  = dataset[:,0] 
Momega_n022  = dataset[:,1]
sepa_n022   = dataset[:,2]


# #SBS star
f130 = open('omega_022_SB.dat', 'w')
dt   = 1.987032418952618396e-01
for i in xrange(1, len(tf4) - 1):
         time = i*dt
         cR2p2   = xf4[i]
         cI2p2   = i_xf4[i]
  	 dtcR2p2 = (xf4[i+1] - xf4[i-1]) / (2.0*dt)
         dtcI2p2 = (i_xf4[i+1] - i_xf4[i-1]) / (2.0*dt)
         temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
         omega2 = abs((1.0/2.0)*temp1)
         f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()


dataset = np.genfromtxt(fname='omega_022_SB.dat')
tomg_sbs_22   = dataset[:,0]/1.46
omega_022_sbs = 1.46*dataset[:,1]

# np.savetxt('omega_022_SB.dat', np.c_[tomg_sbs_22,omega_022_sbs] )


# ## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 50#250
omg_av_sbs_22 = movingaverage(omega_022_sbs, r1)

# np.savetxt('omega_av_SBS.dat', np.c_[tomg_sbs_22,omg_av_sbs_22] )



# #Dark star
f130 = open('omega_022_1.dat', 'w')
dt   = 0.6
for i in xrange(1, len(tt4_ds) - 1):
         time = i*dt
         cR2p2   = xff4_ds[i]
         cI2p2   = i_xff4_ds[i]
 	 dtcR2p2 = (xff4_ds[i+1] - xff4_ds[i-1]) / (2.0*dt)
         dtcI2p2 = (i_xff4_ds[i+1] - i_xff4_ds[i-1]) / (2.0*dt)
         temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
         omega2 = abs((1.0/2.0)*temp1)
         f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_022_1.dat')
tomg_22   = dataset[:,0]/1.46
omega_022 = 1.46*dataset[:,1]

# np.savetxt('omega_022.dat', np.c_[tomg_22,omega_022] )


# ## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 50
omg_av_22 = movingaverage(omega_022, r1)

# np.savetxt('omega_av_22.dat', np.c_[tomg_22,omg_av_22] )

################# C006 #################################
###### Strain Solitonic Stars
dataset = np.genfromtxt(fname='../../simulations/C006/c2p2B.dat')
t1=dataset[:,0]
x1=dataset[:,1]
y1=dataset[:,2]
#----------------
tf1, strainf1 = strain(t1, x1, y1, 0.006)
xf1           = strainf1.real
i_xf1         = strainf1.imag

np.savetxt('h22R_C006_SBS.out', np.c_[tf1,xf1] )   # x,y,z equal sized 1D arrays

###### Strain Dark Stars
dataset = np.genfromtxt(fname='../../simulations/DS/C006/sphere50/c2p2.dat')
tt1_ds=dataset[:,0]
x1_ds=dataset[:,1]
y1_ds=dataset[:,2]

#----------------
ttf1_ds, strain1f1_ds = strain(tt1_ds, x1_ds, y1_ds, 0.006)
xff1_ds         = strain1f1_ds.real
i_xff1_ds       = strain1f1_ds.imag

#np.savetxt('h22R_C012_DS.out', np.c_[ttf2_ds,xff2_ds] )   # x,y,z equal sized 1D arrays

###### h22 tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidal/h22.dat')
t1_006  = dataset[:,0] 
h22_006 = dataset[:,1]

###### h22 no tidal
dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidalzero/h22.dat')
t1_n_006  = dataset[:,0] 
h22_n_006 = dataset[:,1]

#### Sepa tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidal/t_omega_r.dat')
t1_sep_006  = dataset[:,0] 
Momega_006  = dataset[:,1]
sepa_006   = dataset[:,2]

#### Sepa no tidal 
dataset = np.genfromtxt(fname='../../simulations/DS/C006/PN_tidalzero/t_omega_r.dat')
t1_sep_n006  = dataset[:,0] 
Momega_n006  = dataset[:,1]
sepa_n006   = dataset[:,2]


#SBS star
f130 = open('omega_006_SB.dat', 'w')
dt   = 1.997799779977997847e-01
for i in xrange(1, len(tf1) - 1):
        time = i*dt
        cR2p2   = xf1[i]
        cI2p2   = i_xf1[i]
	dtcR2p2 = (xf1[i+1] - xf1[i-1]) / (2.0*dt)
        dtcI2p2 = (i_xf1[i+1] - i_xf1[i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_006_SB.dat')
tomg_sbs_006      = dataset[:,0]/1.07
omega_006_sbs = 1.07*dataset[:,1]

np.savetxt('omega_006_SB.dat', np.c_[tomg_sbs_006,omega_006_sbs] )


## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 250
omg_av_sbs_006 = movingaverage(omega_006_sbs, r1)

#np.savetxt('omega_av_SBS.dat', np.c_[tomg_sbs,omg_av_sbs] )



#Dark star
f130 = open('omega_006_1.dat', 'w')
dt   = 1.5
for i in xrange(1, len(tt1_ds) - 1):
        time = i*dt
        cR2p2   = xff1_ds[i]
        cI2p2   = i_xff1_ds[i]
	dtcR2p2 = (xff1_ds[i+1] - xff1_ds[i-1]) / (2.0*dt)
        dtcI2p2 = (i_xff1_ds[i+1] - i_xff1_ds[i-1]) / (2.0*dt)
        temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)    
        omega2 = abs((1.0/2.0)*temp1)
        f130.write(str(time) + " " + str(omega2) + "\n")
f130.close()

dataset = np.genfromtxt(fname='omega_006_1.dat')
tomg_006      = dataset[:,0]/1.07
omega_006 = 1.07*dataset[:,1]

#np.savetxt('omega_006.dat', np.c_[tomg_006,omega_006] )


## Frequencia con h22
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

r1 = 250
omg_av_006 = movingaverage(omega_006, r1)

#np.savetxt('omega_av.dat', np.c_[tomg,omg_av] )


dpi= 2.0*np.pi
# Two subplots sharing x axes
f, axarr = pl.subplots(3, sharex=False)

axarr[0].axvline(x=950-50,color='k',ls='--',linewidth = 1)
axarr[0].annotate(r' $t_{contact}$', xy=(950-50, 0.001), xytext=(1200, 0.00099), size=18,arrowprops=dict(arrowstyle="->",connectionstyle="arc3,rad=.2",color='black'))
axarr[0].plot(tomg_006[0:tomg_006.shape[0]-150],omg_av_006[0:omg_av_006.shape[0]-150]/dpi, 'r-',linewidth = 3,label=r'$C=0.06$ DBS')
axarr[0].plot(tomg_sbs_006[0:tomg_sbs_006.shape[0]-300],omg_av_sbs_006[0:omg_av_sbs_006.shape[0]-300]/dpi, 'r:',linewidth = 1,label=r'$C=0.06$ BS')
axarr[0].plot(t1_sep_006-50,Momega_006/dpi, 'm-',linewidth = 1,label=r'T4$\,(\kappa_{tidal}=8420)$')
axarr[0].plot(t1_sep_n006-1500,Momega_n006/dpi, 'c-',linewidth = 1,label=r'BH')
axarr[0].legend(loc='lower right', labelspacing=0.1,prop={'size':10.5},ncol=2)
# axarr[0].set_xlabel('t',fontsize=15)
axarr[0].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[0].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[0].set_xlim([600.0,3000])
axarr[0].set_ylim([0.0,0.012])    
for label in axarr[0].get_yticklabels()[1::2]:
     label.set_visible(False) 
axarr[0].set_ylabel(r'$M_{0}\,f_{GW}$',fontsize=20)


axarr[1].axvline(x=300-50,color='k',ls='--',linewidth = 1)
axarr[1].annotate(r' $t_{contact}$', xy=(300-50, 0.001), xytext=(340-50, 0.002), size=18,arrowprops=dict(arrowstyle="->",connectionstyle="arc3,rad=.2",color='black'))
axarr[1].plot(tomg,omg_av/dpi, 'b-',linewidth = 3,label=r'$C=0.12$ DBS')
axarr[1].plot(tomg_sbs[0:tomg_sbs.shape[0]-300],omg_av_sbs[0:omg_av_sbs.shape[0]-300]/dpi, 'b:',linewidth = 1,label=r'$C=0.12$ BS')
axarr[1].plot(t1_sep_012-50,Momega_012/dpi, 'm-',linewidth = 1,label=r'T4$\,(\kappa_{tidal}=332)$')
axarr[1].plot(t1_sep_n012-140,Momega_n012/dpi, 'c-',linewidth = 1,label=r'BH')
# #axarr[0].plot(t1_012-27, 1.7*h22_012, 'b--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=332$')
# #axarr[0].plot(t1_n_012-27, 1.7*h22_n_012, 'g--',linewidth = 1,label=r'$C=0.12$ PN $\kappa_{2}=0$')
axarr[1].legend(loc='lower right', labelspacing=0.1,prop={'size':10.5},ncol=2)
#axarr[0].set_xlabel('t',fontsize=15)
axarr[1].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[1].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[1].set_xlim([131.0,600])
axarr[1].set_ylim([0.0,0.02])    
for label in axarr[1].get_yticklabels()[1::2]:
     label.set_visible(False) 
axarr[1].set_ylabel(r'$M_{0}\,f_{GW}$',fontsize=20)


axarr[2].axvline(x=218-50,color='k',ls='--',linewidth = 1)
axarr[2].annotate(r' $t_{contact}$', xy=(218-50, 0.042), xytext=(218-120, 0.029), size=18,arrowprops=dict(arrowstyle="->",connectionstyle="arc3,rad=.2",color='black'))
axarr[2].plot(tomg_22[0:tomg_22.shape[0]-150],omg_av_22[0:omg_av_22.shape[0]-150]/dpi, 'g-',linewidth = 3,label=r'$C=0.22$ DBS')
axarr[2].plot(tomg_sbs_22[0:omg_av_sbs_22.shape[0]-318],omg_av_sbs_22[0:omg_av_sbs_22.shape[0]-318]/dpi, 'g:',linewidth =1 ,label=r'$C=0.22$ BS')
axarr[2].plot(t1_sep_022-170,Momega_022/dpi, 'm-',linewidth = 1,label=r'T4$\,(\kappa_{tidal}=20)\,$')
axarr[2].plot(t1_sep_n022-310,Momega_n022/dpi, 'c-',linewidth = 1,label=r'BH')
axarr[2].legend(loc='lower right', labelspacing=0.1,prop={'size':10.5},ncol=2)
axarr[2].set_xlabel(r'$t/M_{0}$',fontsize=15)
axarr[2].tick_params(axis='y', which='major',  labelsize=13.5)
axarr[2].tick_params(axis='x', which='major',  labelsize=13.5)
axarr[2].set_xlim([70.0,450])
axarr[2].set_ylim([0.0,0.047])    
for label in axarr[2].get_yticklabels()[1::2]:
     label.set_visible(False) 
axarr[2].set_ylabel(r'$M_{0}\,f_{GW}$',fontsize=20)


pl.tight_layout(pad=0.1)
plt.subplots_adjust(wspace=0.5, hspace=0.2)
#pl.tight_layout()

pl.draw()

pl.savefig("orbital_c006_12_22.pdf", bbox_inches="tight")






